// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// Finite difference grid
// grids are created only within 'solve', not created by the user

// for now: a grid object is what actually owns the grid parameters
class Grid {

public:

	arma::uvec ell0_idx; // indices of x which are for ell0
	arma::uvec ell1_idx; // indices of x which are for ell1

	arma::uvec t_idx; // truncated once from the head

	arma::uvec idx_t; // truncated once from the tail

	arma::uvec t_idx_t; // truncated once from both ends


	arma::colvec ue; // even J grid
	arma::colvec uo; // odd J grid


	arma::sword EVEN = 0;
	arma::sword ODD = 1;

	double Du; // finite uniform grid difference in J

	

};




