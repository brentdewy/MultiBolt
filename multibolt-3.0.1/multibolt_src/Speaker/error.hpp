// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"


// Whether or not print should be allowed to happen
bool print_err_is_allowed() {
	if (mb::mbSpeaker.get_outOp() != mb::OutputOption::SILENT) {
		return true;
	}
	return false;
}

void use_no_more_than_one_eff() {
	std::string str = "MultiBolt disallows using more than one effective momentum transfer Xsecs in the same species at the same time. Use no more than one.";

	if (true /*lib::print_err_is_allowed()*/) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}

void use_only_ela_or_eff() {
	std::string str = "MultiBolt disallows using both elastic momentum transfer and effective momentum transfer Xsecs in the same species at the same time. Use only one or the other.";

	if (true /*lib::print_err_is_allowed()*/) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}



void grid_nan_ocurred() {

	std::string str = "Gridding procedure failed such that nan exists in at least one Xsec. Solution cannot continue.";

	if (true /*lib::print_err_is_allowed()*/) {
		lib::error_statement(str);
	}

	throw std::runtime_error(str);
}



void throw_err_statement(std::string str) {

	lib::error_statement(str);

	throw std::runtime_error(str);

}