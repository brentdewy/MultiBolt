// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// structure for an actual solution
void mb::BoltzmannSolver::execute() {


	for (int i = 0; i < p.remap_grid_trial_max; ++i) {
		if (p.model == mb::ModelCode::HD || p.model == mb::ModelCode::HDGE) {
			solve_f0_HD();
		}
		else if (p.model == mb::ModelCode::SST) {
			solve_f0_SST();
		}

		if (p.ENERGY_REMAP == mb::YesNo::Yes) {
			mb::RemapChoice op = check_grid();
			if (op == mb::RemapChoice::LowerGrid) {
				mb::normal_statement("Tail of f0 looks small; grid maximum is being lowered. f0 will be solved again.");
				lower_grid();
			}
			else if (op == mb::RemapChoice::RaiseGridLittle) {
				mb::normal_statement("Tail of f0 looks large; grid minimum is being raised. f0 will be solved again.");
				raise_grid_little();
			}
			else if (op == mb::RemapChoice::RaiseGridLots) {
				mb::normal_statement("Tail of f0 looks very large; grid minimum is being raised lots. f0 will be solved again.");
				raise_grid_lots();
			}
			else {

				break; // grid is fine, can be left alone
			}
		}
		else {
			break; // don't fuss with the grid, leave
		}

		if (i == p.remap_grid_trial_max) {
			mb::normal_statement("Grid trial maximum reached, no more will be tested.");
		}
	}


	// todo: verify whether or not you should be allowed to use GE with only 2 term expansion
	if (p.model == mb::ModelCode::HDGE) {

#ifdef MULTIBOLT_USING_OPENMP

		#pragma omp parallel sections
		{

			#pragma omp section
			solve_f1T();

			#pragma omp section
			solve_f1L();
		}

		// Calculate second set of gradient expansion systems
		#pragma omp parallel sections
		{
			#pragma omp section
			solve_f2T();

			#pragma omp section
			solve_f2L();
		}
#else

		// totally without openmp
		solve_f1T();
		solve_f1L();

		solve_f2T();
		solve_f2L();

#endif


	}

}