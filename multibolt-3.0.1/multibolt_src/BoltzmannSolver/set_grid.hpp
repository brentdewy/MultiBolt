// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// Set the grids on every cross section to that of ue and uo
void mb::BoltzmannSolver::set_grid() {


	this->g = mb::Grid(); // reset new grid

	g.Du = (present_eV_max / (p.Nu - 1)) * mb::QE;

	arma::vec temp_ue = arma::linspace(0.5 * g.Du, g.Du * (double(p.Nu) - 0.5), p.Nu);
	arma::vec temp_uo = arma::linspace(g.Du, p.Nu * g.Du, p.Nu);
	g.ue = arma::colvec(p.Nu, arma::fill::zeros);
	g.uo = arma::colvec(p.Nu, arma::fill::zeros);

	// had issues setting the vector arma-style, dummy fix
	for (int i = 0; i < p.Nu; ++i) {
		g.ue.at(i) = temp_ue.at(i);
		g.uo.at(i) = temp_uo.at(i);
	}



	g.ell0_idx = arma::regspace<arma::uvec>(0, 1, p.Nu - 1); // 0, 1, 2.....Nu-1 -> corresponds to ell=0 indices
	g.ell1_idx = g.ell0_idx + p.Nu; // Nu, Nu+1, Nu+2.....2*Nu-1 -> corresponds to ell=1 indices

	g.t_idx = arma::regspace<arma::uvec>(1, 1, p.Nu - 1); // truncated once from zero // useful for (k, k-1)
	g.idx_t = arma::regspace<arma::uvec>(0, 1, p.Nu - 2);// truncated once from u_max //  useful for (k+1, k)
	g.t_idx_t = arma::regspace<arma::uvec>(1, 1, p.Nu - 2);


	mb::debug_statement("Applying even and odd grids (linear-spaced, per-energy).");

	for (auto& spec : lib.allspecies) {

		for (auto& x : spec->allcollisions) {
			x->set_grid(g.EVEN, g.ue / mb::QE);
			x->set_grid(g.ODD, g.uo / mb::QE);
		}
	}




	// check for nan entries in grids
	// if this error flips, it is likely a development error
	for (auto& spec : lib.allspecies) {
		for (auto& x : spec->allcollisions) {

			if (x->gridded_s[g.EVEN].has_nan() || x->gridded_s[g.ODD].has_nan()) {
				mb::throw_err_statement("Solution cannot continue: Gridding procedure failed such that nan exists in at least one Xsec.");

			}
		}
	}



	// Based on this grid, figure out if iterations are actually needed
	// in order to not waste time iterating

	int how_many_iz_outside = 0;
	int how_many_iz = 0;
	int how_many_att = 0;

	for (auto& spec : lib.allspecies) {
		for (auto& x : spec->iz) {

			how_many_iz++;
			if (x->eV_thresh() > present_eV_max) {
				how_many_iz_outside++;
			}
		}
		for (auto& x : spec->att) {
			how_many_att++;
		}
	}

	if ((how_many_iz_outside == how_many_iz) && (how_many_att == 0)) {
		NO_ITERATION = true;
		mb::debug_statement("Detected that grid is all-conservative processes: all calculations will be single-iteration.");
	}


	mb::normal_statement("Begin loading collision matrix.");
	
	// pre-load the scattering matrix.

	this->A_scattering = arma::SpMat<double>(p.Nu * p.N_terms, p.Nu * p.N_terms); // set to zeros
	for (int ell = 0; ell < p.N_terms; ++ell) {
		full_collision_operator(ell, this->A_scattering);
	}

	//mb::normal_statement("Finishing loading collision matrix.");
};


