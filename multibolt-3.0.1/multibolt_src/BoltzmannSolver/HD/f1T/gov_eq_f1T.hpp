// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"



// The following are simimlar to the case of f_0, but use different factors of ell!

// governing equation for terms of (ell + 1) (f1T)
double eq_f1T_ell_plus_one(double u, int ell, double E0, double Np, double Du, bool is_forward) {
	int SIGN = is_forward ? +1 : -1;
	return (SIGN * mb::QE * E0 * (ell + 2.0) / (2.0 * ell + 3.0) * u / Du
		+ mb::QE * E0 * (ell + 2.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2.0 / 2.0);

};

// governing equation for terms of (ell - 1) (f1T)
double eq_f1T_ell_minus_one(double u, int ell, double E0, double Np, double Du, bool is_forward) {
	int SIGN = is_forward ? +1 : -1;
	return (SIGN * mb::QE * E0 * (ell - 1.0) / (2.0 * ell - 1.0) * u / Du
		- mb::QE * E0 * (ell - 1.0) / (2.0 * ell - 1.0) * (ell - 1.0) / 2.0 / 2.0);

};



void mb::BoltzmannSolver::gov_eq_f1T(int ell, arma::SpMat<double>& A, arma::Col<double>& b) {


	arma::colvec u;
	bool forward = true;
	bool backward = false;

	if (ell % 2 == 0 || ell == 0) { // if is even
		u = g.ue;
	}
	else { // if is odd instead
		u = g.uo;

		// flip derivative direction
		forward = !(forward);
		backward = !(backward);
	}


	for (arma::sword k = 0; k < p.Nu; ++k) {
		// set index for first derivative effect
		arma::sword kcol = k;

		if (ell % 2 == 0 || ell == 0) { // if is even
			kcol = k - 1;
		}
		else { // if is odd
			kcol = k + 1;
		}

		// fill in a manner similar to that for f0_HD
		// Apply for f_ell
		A.at(k + (ell * p.Nu), k + (ell * p.Nu)) += eq_f0_HD_ell(u(k), omega0, Np); // okay to reuse expression


		// Apply for ell+1
		if (ell + 1 < p.N_terms) {	// if ell+1 exists
			// f_(l + 1)(k)
			A.at(k + (ell * p.Nu), k + (ell + 1) * p.Nu) += eq_f1T_ell_plus_one(u(k), ell, E0, Np, g.Du, forward);

			if (kcol >= 0 && kcol < p.Nu) { // if kcol index exists
				// f_(l + 1)(k - 1)
				A.at(k + (ell * p.Nu), (kcol)+(ell + 1) * p.Nu) += eq_f1T_ell_plus_one(u(k), ell, E0, Np, g.Du, backward);
			}
		}

		// Apply for ell - 1
		if (ell - 1 >= 0) {	// if ell-1 exists
			// f_(l - 1)(k)
			A.at(k + (ell * p.Nu), k + (ell - 1) * p.Nu) += eq_f1T_ell_minus_one(u(k), ell, E0, Np, g.Du, forward);

			if (kcol >= 0 && kcol < p.Nu) { // if kcol index exists 
				// f_(l - 1)(k + 1)
				A.at(k + (ell * p.Nu), (kcol)+(ell - 1) * p.Nu) += eq_f1T_ell_minus_one(u(k), ell, E0, Np, g.Du, backward);
			}
		}



		// todo: figure out how to better abstract this

		// now fill b vector
		if (1 <= ell && ell <= p.N_terms - 2) {
			if (ell % 2 == 0) {  // ell = 'even'
				if (k != 0) {
					b.at(k + ell * p.Nu) += Np * (
						+1 / (2.0 * ell - 1.0) * u.at(k) * (x_f.at(k + ell * p.Nu - p.Nu) // concerns ell - 1
							+ x_f.at(k + ell * p.Nu - p.Nu - 1)) / 2.0 - 1 / (2.0 * ell + 3.0) * u.at(k) * (x_f.at(k + ell * p.Nu + p.Nu) // concerns ell -1 and then ell + 1
								+ x_f.at(k + ell * p.Nu + p.Nu - 1)) / 2.0); // conernes ell + 1
				}
				else {
					b.at(k + ell * p.Nu) +=
						Np * (+1 / (2.0 * ell - 1.0) * u.at(k) * x_f.at(k + ell * p.Nu - p.Nu) / 2.0 - 1 / (2.0 * ell + 3.0) * u.at(k) * x_f.at(k + ell * p.Nu + p.Nu) / 2.0);
				}
			}
			else {
				if (k != p.Nu - 1) {
					b.at(k + ell * p.Nu) +=
						Np * (+1 / (2.0 * ell - 1.0) * u.at(k) * (x_f.at(k + ell * p.Nu - p.Nu)
							+ x_f.at(k + ell * p.Nu - p.Nu + 1)) / 2.0 - 1 / (2.0 * ell + 3.0) * u.at(k) * (x_f.at(k + ell * p.Nu + p.Nu) + x_f.at(k + ell * p.Nu + p.Nu + 1)) / 2.0);
				}
				else {
					b.at(k + ell * p.Nu) +=
						Np * (+1 / (2.0 * ell - 1.0) * u.at(k) * x_f.at(k + ell * p.Nu - p.Nu) / 2.0 - 1 / (2.0 * ell + 3.0) * u.at(k) * x_f.at(k + ell * p.Nu + p.Nu) / 2.0);
				}
			}
		}
		else if (ell == p.N_terms - 1) {
			if (k != p.Nu - 1) {
				b.at(k + ell * p.Nu) +=
					Np * (+1 / (2.0 * ell - 1.0) * u.at(k) * (x_f.at(k + ell * p.Nu - p.Nu) + x_f.at(k + ell * p.Nu - p.Nu + 1)) / 2.0); //concerns ell - 1
			}
			else {
				b.at(k + ell * p.Nu) +=
					Np * (+1 / (2.0 * ell - 1.0) * u.at(k) * x_f.at(k + ell * p.Nu - p.Nu) / 2.0); // concerns ell - 1
			}
		}
	}

}
