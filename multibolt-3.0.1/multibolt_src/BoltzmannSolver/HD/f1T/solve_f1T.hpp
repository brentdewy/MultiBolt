// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// solve governing equation for 1T gradient
void mb::BoltzmannSolver::solve_f1T() {

	mb::debug_statement("Begin solving governing equation for f1T.");

	arma::uword N = p.Nu * p.N_terms;

	arma::SpMat<double> A(N, N);
	// special consideration for f1T: only the ell != 0 submats matter
	// resizing will occur later, full size is kept to match the collision operator functions

	arma::colvec b(N, arma::fill::zeros);

	x_f1T = arma::colvec(N, arma::fill::zeros);

	// Note: f1T has a direct solution and needs only one iteration
	// and is unfilled for ell == 0!
	for (arma::uword ell = 1; ell < p.N_terms; ++ell) {
		gov_eq_f1T(ell, A, b);
	}
	A = A - A_scattering;


	arma::uvec idx = arma::regspace<arma::uvec>((p.Nu + 1), N, 1);


	// resize for actual solution
	A = A.tail_rows(N - p.Nu);
	A = A.tail_cols(N - p.Nu);
	b = b.tail_rows(N - p.Nu);

	
	

	x_f1T = arma::colvec(N - p.Nu, arma::fill::zeros);

	// no normalization condition : f0_1L is not relevant here

	// Scaling not quite necessary?
	 //b = b / arma::max(arma::max(A));
	 //A = A/ arma::max(arma::max(A));
	

	x_f1T = arma::spsolve(A, b, "superlu", mb::multibolt_superlu_opts()); // solve

	mb::normal_statement("f_1T solved in single iteration.");
	mb::display_iteration_banner("f1T", 1, "DFTN [m^-1 s^-1]", calculate_DFTN(), 0);

	mb::debug_statement("Exit solving governing equation for f1T.");

}