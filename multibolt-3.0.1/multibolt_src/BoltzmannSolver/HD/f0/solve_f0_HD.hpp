// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// solve governing equation for f0 in HD conditions
// This is not the same equation as for SST
void mb::BoltzmannSolver::solve_f0_HD() {

	mb::debug_statement("Begin solving governing equation for f0 (HD).");

	omega0 = 0; // initial guess for omega0

	arma::uword N = p.Nu * p.N_terms;


	arma::Col<double> b(N, arma::fill::zeros);	// solve-against vector (contains normalization condition)
	x_f = arma::colvec(N, arma::fill::zeros);						// solution vector (contains distribution functions)
	arma::SpMat<double> A(N, N);				// coefficient matrix

	// initialize "previous values"
	double omega0_prev = 0; // previous value, tracking for convergence
	double avg_en_prev = 0;

	bool CONVERGED = false;

	for (iter_f0_HD = 1; iter_f0_HD <= p.iter_min || (iter_f0_HD < p.iter_max && !CONVERGED); ++iter_f0_HD) {

		A.zeros();
		b.zeros();

		for (int ell = 0; ell < p.N_terms; ++ell) {
			gov_eq_f0_HD(ell, A);
		}
		A = A - A_scattering;


		// Normalization condition
		A.row(0).zeros();
		A.head_cols(p.Nu).row(0) = sqrt(g.ue / mb::QE).t() * g.Du / mb::QE;
		b.at(0) = 1.0;



		// Boundary conditions
		// A note: schemes beyond _0 or SST are insensitive to boundary conditions
		// (odd BC is subtly incorporated in the discretization scheme and does not require an explicit fixed point)
		for (int ell = 1; ell < p.N_terms; ++ell) {
			if (ell % 2 == 0) { //% ell = (even), setup even BC

				A.row((ell+1) * p.Nu - 1).zeros();
				A((ell+1) * p.Nu - 1, (ell+1) * p.Nu - 1) = 1.0;
			}
			else { // if is odd instead // <- do not add in, unnescessary
				//A.row((ell+1) * p.Nu - 1).zeros();
				//A((ell+1) * p.Nu - 1, ell * p.Nu) = 1.0;
			}
		}
		

		avg_en_prev = calculate_avg_en();

		x_f = arma::spsolve(A, b, "superlu", multibolt_superlu_opts()); // solve

		double avg_en = calculate_avg_en();


		omega0_prev = omega0;
		omega0 = calculate_k_iz_eff_N(); // by definition, omega0 = k_iz_eff_N
		omega0 = (1.0 - p.weight_f0) * omega0_prev + p.weight_f0 * omega0;


		//A.brief_print();

		// check if ionization is ocurring at all - converge against avg_en instead if need be
		if (this->NO_ITERATION || omega0 == 0) {
			mb::normal_statement("Single iteration case: no ionization or attachment found.");
			mb::display_iteration_banner("f0", iter_f0_HD, "avg_en [eV]", avg_en, 0);
			CONVERGED = true;
			break;
		}
		else {
			mb::display_iteration_banner("f0", iter_f0_HD, "omega0 [m**-3 s^-1]", omega0, err(omega0, omega0_prev));
			CONVERGED = mb::converged(p.conv_err, omega0, omega0_prev);
		}
	}

	mb::check_did_not_converge("f0", iter_f0_HD, p.iter_max);

	mb::debug_statement("Exit solving governing equation for f0 (HD).");
	

}


