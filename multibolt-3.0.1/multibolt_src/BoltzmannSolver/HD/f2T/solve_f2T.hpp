// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

void mb::BoltzmannSolver::solve_f2T() {

	mb::debug_statement("Begin solving governing equation for f2T.");

	// initial guess, not actually omega2
	omega2 = (2.0 * calculate_DFTN() + calculate_DFLN()) / sqrt(3.0) / Np;

	arma::uword N = p.N_terms * p.Nu;
	arma::sp_mat A(N, N);
	arma::colvec b(N, arma::fill::zeros);

	x_f2T = arma::colvec(N, arma::fill::zeros);

	bool CONVERGED = false;

	for (iter_f2T = 1; iter_f2T <= p.iter_min || (iter_f2T < p.iter_max && !CONVERGED); ++iter_f2T) {

		A.zeros();
		b.zeros();
		x_f2T.zeros();


		for (int ell = 0; ell < p.N_terms; ++ell){
			gov_eq_f2T(ell, A, b);
		}
		A = A - A_scattering;

		b = b * Np;

		// Normalization condition
		A.row(p.Nu - 1).zeros();
		A.head_cols(p.Nu).row(p.Nu - 1) = (sqrt(g.ue / mb::QE) * g.Du / mb::QE).t();
		b.at(p.Nu - 1) = 0;

		x_f2T = arma::spsolve(A, b, "superlu", mb::multibolt_superlu_opts()); // solve

		double omega2_prev = omega2;

		double S_2T = (calculate_total_S(x_f2T(g.ell0_idx), lib::CollisionCode::ionization) / Np - calculate_total_S(x_f2T(g.ell0_idx), lib::CollisionCode::attachment) / Np);
		
		//update
		omega2 = (2.0 * calculate_DFTN() + calculate_DFLN()) / sqrt(3.0) / Np + S_2T;

		// check if ionization is ocurring at all
		if (this->NO_ITERATION || omega0 == 0) {
			mb::normal_statement("Single iteration case: no ionization or attachment found.");
			mb::display_iteration_banner("f2T", iter_f2L, "omega2 [m**2 s**-1]", omega2, 0);
			CONVERGED = true;
			break;
		}
		else {
			mb::display_iteration_banner("f2T", iter_f2T, "omega2 [m**2 s**-1]", omega2, err(omega2, omega2_prev));
			CONVERGED = mb::converged(p.conv_err, omega2, omega2_prev);
		}
	}

	mb::check_did_not_converge("f2T", iter_f2T, p.iter_max);

	mb::debug_statement("Exit solving governing equation for f2T.");
}
