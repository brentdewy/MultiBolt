// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"



void mb::BoltzmannSolver::gov_eq_f1L(int ell, arma::SpMat<double>& A, arma::Col<double>& b) {


	gov_eq_f0_HD(ell, A);

	mb::debug_statement("Applying governing equation associated with f0 (HD)");

	arma::colvec u;

	if (ell % 2 == 0 || ell == 0) {
		u = g.ue;
	}
	else {
		u = g.uo;
	}

	// todo: figure out how to better abstract this

	for (arma::sword k = 0; k < p.Nu; ++k) {

		if (ell == 0) {
			if (k != 0) {
				b(k + ell * p.Nu) +=
					-Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k)
					+ Np * (ell + 1.0) / (2.0 * ell + 3.0) * u(k) * (x_f(k + p.Nu) + x_f(k + p.Nu - 1)) / 2.0;
			}
			else {
				b(k + ell * p.Nu) +=
					-Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k)
					+ Np * (ell + 1.0) / (2.0 * ell + 3.0) * u(k) * x_f(k + p.Nu) / 2.0;
			}
		}
		else if (1 <= ell && ell <= p.N_terms - 2) {
			if (ell % 2 == 0) {//% l1 = 'even'
				if (k != 0) {
					b(k + ell * p.Nu) +=
						-Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k + ell * p.Nu)
						+ Np * ell / (2.0 * ell - 1.0) * u(k) * (x_f(k + ell * p.Nu - p.Nu) + x_f(k + ell * p.Nu - p.Nu - 1)) / 2.0
						+ Np * (ell + 1.0) / (2.0 * ell + 3.0) * u(k) * (x_f(k + ell * p.Nu + p.Nu) + x_f(k + ell * p.Nu + p.Nu - 1)) / 2.0;
				}
				else {
					b(k + ell * p.Nu) +=
						-Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k + ell * p.Nu)
						+ Np * ell / (2.0 * ell - 1.0) * u(k) * x_f(k + ell * p.Nu - p.Nu) / 2.0
						+ Np * (ell + 1.0) / (2.0 * ell + 3.0) * u(k) * x_f(k + ell * p.Nu + p.Nu) / 2.0;
				}
			}
			else {
				if (k != (p.Nu - 1)) {
					b(k + ell * p.Nu) +=
						-Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k + ell * p.Nu)
						+ Np * ell / (2.0 * ell - 1.0) * u(k) * (x_f(k + ell * p.Nu - p.Nu) + x_f(k + ell * p.Nu - p.Nu + 1)) / 2.0
						+ Np * (ell + 1.0) / (2.0 * ell + 3.0) * u(k) * (x_f(k + ell * p.Nu + p.Nu) + x_f(k + ell * p.Nu + p.Nu + 1)) / 2.0;

				}
				else {
					b(k + ell * p.Nu) +=
						-Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k + ell * p.Nu)
						+ Np * ell / (2.0 * ell - 1.0) * u(k) * x_f(k + ell * p.Nu - p.Nu) / 2.0
						+ Np * (ell + 1.0) / (2.0 * ell + 3.0) * u(k) * x_f(k + ell * p.Nu + p.Nu) / 2.0;
				}
			}
		}
		else {
			if (k != (p.Nu - 1)) {
				b(k + ell * p.Nu) += -Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k + ell * p.Nu)
					+ Np * ell / (2.0 * ell - 1.0) * u(k) * (x_f(k + ell * p.Nu - p.Nu) + x_f(k + ell * p.Nu - p.Nu + 1)) / 2.0;
			}
			else {
				b(k + ell * p.Nu) += -Np * sqrt(u(k) * mb::ME / 2.0) * omega1 * x_f(k + ell * p.Nu)
					+ Np * ell / (2.0 * ell - 1.0) * u(k) * x_f(k + ell * p.Nu - p.Nu) / 2.0;

			}
		}
	}

}