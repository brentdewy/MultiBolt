// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"


void mb::BoltzmannSolver::gov_eq_f2L(int ell, arma::SpMat<double>& A, arma::colvec& b) {

	gov_eq_f0_HD(ell, A);


	arma::colvec u;


	if (ell % 2 == 0 || ell == 0) { // if is even
		u = g.ue;
	}
	else { // if is odd instead
		u = g.uo;
	}

	// todo: figure out how to better abstract this
	for (arma::sword k = 0; k < p.Nu; ++k) {
		if (ell == 0) {
			if (k != 0) {
				b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
					+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
					- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu + p.Nu) + x_f1L(k + ell * p.Nu + p.Nu - 1)) / 2
					+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu) + x_f1T(k + ell * p.Nu - 1)) / 2;
			}
			else {
				b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
					+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
					- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu + p.Nu) / 2
					+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu) / 2;
			}
		}
		else if (ell == 1) {
			if (k != p.Nu - 1) {

				if (k + ell * p.Nu < ell * p.Nu && k + ell * p.Nu >= 0) {
					b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
						+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
						- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu - p.Nu) + x_f1L(k + ell * p.Nu - p.Nu + 1)) / 2
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu + p.Nu) + x_f1L(k + ell * p.Nu + p.Nu + 1)) / 2
						+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu) + x_f1T(k + ell * p.Nu + 1)) / 2;
				}

				
			}
			else {
				if (k + ell * p.Nu < ell * p.Nu && k + ell * p.Nu >= 0) {
				
					b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
						+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
						- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu - p.Nu) / 2
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu + p.Nu) / 2
						+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu) / 2;
				}

				
			}
		}
		else if (2 <= ell && ell <= p.N_terms - 2) {
			if (ell % 2 == 0) { // ell = 'even'
				if (k != 0) {
					b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
						+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
						- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu - p.Nu) + x_f1L(k + ell * p.Nu - p.Nu - 1)) / 2
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu + p.Nu) + x_f1L(k + ell * p.Nu + p.Nu - 1)) / 2
						- ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu - 2 * p.Nu) + x_f1T(k + ell * p.Nu - 2 * p.Nu - 1)) / 2
						+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu) + x_f1T(k + ell * p.Nu - 1)) / 2;
				}
				else {
					b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
						+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
						- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu - p.Nu) / 2
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu + p.Nu) / 2
						- ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu - 2 * p.Nu) / 2
						+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu) / 2;
				}
			}
			else {// ell = 'odd'
				if (k != p.Nu - 1) {
					b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
						+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
						- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu - p.Nu) + x_f1L(k + ell * p.Nu - p.Nu + 1)) / 2
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu + p.Nu) + x_f1L(k + ell * p.Nu + p.Nu + 1)) / 2
						- ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu - 2 * p.Nu) + x_f1T(k + ell * p.Nu - 2 * p.Nu + 1)) / 2
						+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu) + x_f1T(k + ell * p.Nu + 1)) / 2;
				}
				else {
					b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
						+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
						- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu - p.Nu) / 2
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu + p.Nu) / 2
						- ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu - 2 * p.Nu) / 2
						+ (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu) / 2;
				}
			}
		}
		else if (ell == p.N_terms - 1) {
			if (k != p.Nu - 1) {
				b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
					+ sqrt(u(k) * mb::ME / 2) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
					- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * (x_f1L(k + ell * p.Nu - p.Nu) + x_f1L(k + ell * p.Nu - p.Nu + 1)) / 2
					- ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2 * sqrt(2.0 / 3.0) * u(k) * (x_f1T(k + ell * p.Nu - 2 * p.Nu) + x_f1T(k + ell * p.Nu - 2 * p.Nu + 1)) / 2;
			}
			else {
				b(k + ell * p.Nu) += -sqrt(u(k) * mb::ME / 2) * omega2_bar * x_f(k + ell * p.Nu) * Np
					+ sqrt(u(k) * mb::ME / 2.0) * sqrt(2.0 / 3.0) * omega1 * x_f1L(k + ell * p.Nu)
					- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u(k) * x_f1L(k + ell * p.Nu - p.Nu) / 2
					- ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2 * sqrt(2.0 / 3.0) * u(k) * x_f1T(k + ell * p.Nu - 2 * p.Nu) / 2;
			}
		}
	}


}