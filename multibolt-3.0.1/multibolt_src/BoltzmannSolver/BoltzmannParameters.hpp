// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include"multibolt"


// Parameters are struct-like


class BoltzmannParameters  {

	public:

		std::vector<std::string> argslist; // if MultiBolt was called with arguments, copy those arguments here

		int Nu = 1000; // grid points
		int N_terms = 6; // expansion terms
		
		mb::ModelCode model = mb::ModelCode::HDGE; // hydrodynamic, hydro+gradient-expansion, or steady-state-townsend


		mb::YesNo ENERGY_REMAP = mb::YesNo::No; // activate/disable grid remapping
	
		mb::YesNo use_eV_max_guess = mb::YesNo::No; // empiric guess: say you start eV_max = E/N [Td]

		int iter_max = 100; // allowable iterations per scheme
		int iter_min = 10;

		double conv_err = 1e-6; // convergence error target for the interable coefficient

		double initial_eV_max = 100; // roof of grid considered - rule of thumb, this should be high enough such that the EEDF tail is ~10 orders of magnitude down from the head

		double p_Torr = 760; // pressure [Torr] - used for calculating Np
		double T_K = 300; // temperature [T_K] - thermal dependence 

		double EN_Td = 100; // density reduled electric field strength in Td=1e-21 Vm^2

		int remap_grid_trial_max = 10; // try a new grid no more than this many times

		double remap_target_order_span = 10.0; // how many orders would you prefer your EEDF to span (in eV^-3/2)

		double weight_f0 = 1.0; // non-conservative swarms (iz + att)

		double sharing = 0.5; // energy split between the primary and secondary. 0.5 means equal sharing, 0 means one-takes-all. Use in range [0 and 0.5].

		// For now, scattering is treated as per collision-type
		// in the future, it will need to be based per-cross section
		mb::Scattering elastic_scattering = mb::isotropic_scattering();
		mb::Scattering excitation_scattering = mb::isotropic_scattering();
		mb::Scattering ionization_scattering = mb::isotropic_scattering();
		mb::Scattering superelastic_scattering = mb::isotropic_scattering();


		void print() {

			mb::normal_statement(print_str());
		};

		std::string print_str() {
			std::stringstream ss; ss.str("");
			ss.setf(std::ios_base::scientific, std::ios_base::floatfield);
			ss << "\n";
			ss << "\tN points: " << Nu << "\n";
			ss << "\tN terms: " << N_terms << "\n";
			ss << "\tModel: " << mb::model_map.find(model)->second << "\n";
			ss << "\tRemap Energy?: " << mb::yesno_map.find(ENERGY_REMAP)->second << "\n";

			ss << "\titer_min: " << iter_min << "\n";
			ss << "\titer_max: " << iter_max << "\n";

			ss << "\tConv. Err: " << std::setprecision(4) << conv_err << "\n";
			ss << "\tMax eV: " << std::setprecision(4) << initial_eV_max << "\n";

			ss << "\tPressure [torr]: " << std::setprecision(4) << p_Torr << "\n";
			ss << "\tTemp. [K]: " << std::setprecision(4) << T_K << "\n";

			ss << "\tE/N [Td]: " << std::setprecision(4) << EN_Td << "\n";

			ss << "\tMax grid trials: " << remap_grid_trial_max << "\n";

			ss << "\tEEDF Order Span (target): " << std::setprecision(4) << remap_target_order_span << "\n";
			ss << "\tWeight (f0): " << std::setprecision(4) << weight_f0 << "\n";

			ss << "\tScattering Models: " << "\n";
			ss << "\t\tElastic: " << elastic_scattering.name << "\n";
			ss << "\t\tExcitation: " << excitation_scattering.name << "\n";
			ss << "\t\tIonization: " << ionization_scattering.name << "\n";
			ss << "\t\tSuperelastic: " << superelastic_scattering.name << "\n";


			ss << "\tIonization energy sharing: " << std::setprecision(4) << sharing << "\n";

			return ss.str();
		};
};

