// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// The purpose of this file is to hold the functions which are used to calculate all the things
// that MultiBolt cares about


// for SST cases, this is equivalent to W_f0_SST
// for HD cases, this is equivalent to W_FLUX
double mb::BoltzmannSolver::calculate_W_f0() {

	// original implementation
	// reason for edge truncation is unclear
	//arma::colvec eV = g.uo(g.idx_t) / mb::QE;
	//arma::colvec f = x_f(g.ell1_idx(g.idx_t)); // take from first anisotropy (truncated from edge)

	arma::colvec eV = g.uo / mb::QE;
	arma::colvec f = x_f(g.ell1_idx); // take from first anisotropy (truncated from edge)

	return 1.0 / 3.0 * sqrt(2.0  * mb::QE / mb::ME) * arma::accu(trapz(eV, eV % f));
}

// for SST cases, this is equivalent to W_f0_SST
// for HD cases, this is equivalent to W_FLUX
double mb::BoltzmannSolver::calculate_muN_f0() {

	return calculate_W_f0() / (p.EN_Td * 1e-21); // defind using: W = mu * E; W = muN / (E/N)
}

double mb::BoltzmannSolver::calculate_muN_BULK() {

	// defind using: W = mu * E; W = muN / (E/N)
	// ternary operator: return nan if this is the wrong kind of calculation to do this for
	return (p.model == mb::ModelCode::HDGE) ? calculate_W_BULK() / (p.EN_Td * 1e-21) : arma::datum::nan;
}


double mb::BoltzmannSolver::calculate_avg_en() {

	arma::colvec eV = g.ue / mb::QE;
	arma::colvec f = x_f(g.ell0_idx); // take from isotropy

	return arma::accu(trapz(eV, arma::pow(eV, 1.5) % f));
}


double mb::BoltzmannSolver::calculate_alpha_eff_N() {

	double val = arma::datum::nan;

	if (p.model == mb::ModelCode::HDGE) {
		
		double W = calculate_W_BULK();
		double k_N = calculate_k_iz_eff_N();
		double DLN = calculate_DLN_BULK();

		val = W / (2.0 * DLN) - sqrt(pow(W / (2.0 * DLN), 2.0) - k_N / DLN);
	}
	else { // one warning: for HD conditions without GE, this is an estimation at best
		val = calculate_k_iz_eff_N() / calculate_W_f0();
	}

	return val;
}


// Calculate an individual rate coefficient k/N
double mb::BoltzmannSolver::calculate_rate(std::shared_ptr<lib::AbstractXsec> x) {


	//x->gridded_s[g.EVEN].print();
		
	// frac dependence removed at this stage - meaning, no rate is zero even for species with frac = 0.0
	return sqrt(2.0 * mb::QE / mb::ME) * arma::accu(arma::trapz(g.ue / mb::QE, x->gridded_s[g.EVEN] % (g.ue / mb::QE) % x_f(g.ell0_idx)));

}


// Calculate an total rate k/N
double mb::BoltzmannSolver::calculate_total_rate(const lib::CollisionCode& type) {

	double val = 0;

	for (auto& spec : lib.allspecies) {

		double frac = spec->frac();

		auto set = (*spec->pick_from_code(type));

		for (auto& x : set) {
			val = val + calculate_rate(x) * frac;
		}
	}

	return val;
}


// more generalized form of calculating a rate
double mb::BoltzmannSolver::calculate_total_S(const arma::colvec& f, const lib::CollisionCode& type) {

	double val = 0;


	for (auto& spec : lib.allspecies) {

		auto set = (*spec->pick_from_code(type));
		double frac = spec->frac();

		for (auto& x : set) {

			val = val +  sqrt(2.0 * mb::QE / mb::ME) * arma::accu(arma::trapz(g.ue / mb::QE, frac * x->gridded_s[g.EVEN] % (g.ue / mb::QE) % f));
			
		}

	}
	return val;
}



// Calculate an individual growth coefficient X/N
double mb::BoltzmannSolver::calculate_growth(std::shared_ptr<lib::AbstractXsec> x) {

	if (p.model != mb::ModelCode::SST) {
		return arma::datum::nan;
	}


	return calculate_rate(x) / calculate_W_f0();

	
}

double mb::BoltzmannSolver::calculate_total_growth(const lib::CollisionCode& type) {

	return calculate_total_rate(type) / calculate_W_f0();

}


double mb::BoltzmannSolver::calculate_DFTN() {

	
	return (p.model == mb::ModelCode::HDGE) ?
		(1.0 / 3.0) * sqrt(2.0 / mb::ME / mb::QE) * arma::accu(trapz(g.uo / mb::QE, g.uo % x_f1T(g.ell0_idx))) : arma::datum::nan;  // Flux portion of DTN
}

double mb::BoltzmannSolver::calculate_DFLN() {
	return (p.model == mb::ModelCode::HDGE) ?
		(1.0 / 3.0) * sqrt(2.0 / mb::ME / mb::QE) * arma::accu(trapz(g.uo / mb::QE, g.uo % x_f1L(g.ell1_idx))) : arma::datum::nan; // Flux portion of DLN
}

double mb::BoltzmannSolver::calculate_W_BULK() {
	return (p.model == mb::ModelCode::HDGE) ? 
		omega1 : arma::datum::nan;
}

double mb::BoltzmannSolver::calculate_DLN_BULK() {
	return (p.model == mb::ModelCode::HDGE) ?
		(1.0 / sqrt(3.0)) * (omega2 - sqrt(2.0) * omega2_bar) * Np : arma::datum::nan;
}

double mb::BoltzmannSolver::calculate_DTN_BULK() {
	return (p.model == mb::ModelCode::HDGE) ?
		(1.0 / sqrt(3.0)) * (omega2 + 1.0 / sqrt(2.0) * omega2_bar) * Np : arma::datum::nan;
}

double mb::BoltzmannSolver::calculate_k_iz_eff_N() {

	return (calculate_total_rate(lib::CollisionCode::ionization) - calculate_total_rate(lib::CollisionCode::attachment));
}


// Conventional expression of diffusion coefficient
// Cannot account for non-conservative effects
double mb::BoltzmannSolver::calculate_DN_f0() {

	return (1.0 / 3.0) * sqrt(2.0 / mb::ME / mb::QE) * arma::accu(trapz(g.ue / mb::QE, g.ue / calculate_s_Te_eff() % x_f(g.ell0_idx)));
}




// Same as above, but for the neutrals
double mb::BoltzmannSolver::calculate_DN_f0_nu() {
	return (1.0 / 3.0) * sqrt(2.0 / mb::ME / mb::QE) * arma::accu(trapz(g.ue/mb::QE, g.ue /
		(calculate_s_Te_eff() + calculate_k_iz_eff_N() * (1.0 / (g.ue * sqrt(2.0 / mb::QE / mb::ME)))) % x_f(g.ell0_idx)));
}


arma::colvec mb::BoltzmannSolver::calculate_s_Te_eff() {
	
	arma::colvec temp(p.Nu, arma::fill::zeros);


	for (auto& spec : lib.allspecies) {

		for (auto& x : spec->allcollisions) {

			temp += x->gridded_s[g.EVEN] * spec->frac();

		}
	}

	return temp;
}


// EEDF proportional to sqrt(e)*exp(-e/(kB*T)) while EN_Td=zero and T > 0 K
arma::colvec mb::BoltzmannSolver::calculate_MaxwellBoltzmann_EEDF() {

	arma::colvec EEDF(p.Nu, arma::fill::zeros);

	if (COLD_CASE) {
		EEDF = arma::datum::nan;
	}
	else {
		arma::colvec EEDF(p.Nu, arma::fill::zeros);
		EEDF = pow(mb::ME / (2.0 * PI * (mb::KB * p.T_K / mb::QE)), 1.5)  * exp(-(g.ue / mb::QE) / (mb::KB * p.T_K / mb::QE)) * sqrt(2.0 * mb::ME) / pow(mb::ME, 2) * 4 * PI;
	}

	return EEDF;
}

// Characteristic energy using f_T
double mb::BoltzmannSolver::calculate_DT_mu() {
	return (p.model == mb::ModelCode::HDGE) ? calculate_DTN_BULK() / calculate_muN_BULK() : arma::datum::nan;
}

// Characteristic energy using f_L
double mb::BoltzmannSolver::calculate_DL_mu() {
	return (p.model == mb::ModelCode::HDGE) ? calculate_DLN_BULK() / calculate_muN_BULK() : arma::datum::nan;
}

// Characteristic energy using f_0
double mb::BoltzmannSolver::calculate_D_mu() {
	return calculate_DN_f0() / calculate_muN_f0();
}

