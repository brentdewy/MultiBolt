// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// A BoltzmannSolver is the physics owner of most of the math
// Construct one to perform a calculation
// When finished, call .get_output() to receive output object

class BoltzmannSolver {

	public:

		mb::BoltzmannParameters p;
		lib::Library lib; // <- collisionlibrary
		mb::BoltzmannOutput out;

	private:

		
	
		double Np; // neutral particle density m^-3
		double E0; // field strength V/m
		mb::Grid g;

		int iter_f0_SST = 0;

		int iter_f0_HD = 0;
		int iter_f1L = 0;
		int iter_f1T = 0;
		int iter_f2L = 0;
		int iter_f2T = 0;

		//int N_gases = arma::datum::nan;

		// Eigenvalues of numerical schemes
		double alpha_eff_N; // of SST
		double omega0; // of 0
		double omega1; // of 1L
		double omega2; // of 2L
		double omega2_bar; // of 2T


		// Distribution function solutions
		arma::colvec x_f;
		arma::colvec x_f1L;
		arma::colvec x_f1T;
		arma::colvec x_f2L;
		arma::colvec x_f2T;

		// standby scattering matrix - only calculate once per grid.
		// some conditions (many colliosions, anisotropic scattering) make this calculation take longer than usual
		arma::SpMat<double> A_scattering;

		// wall_clock, for tic/toc
		arma::wall_clock timer;

		// special flags
		bool COLD_CASE = false; // flips if p.T_K is found to be zero, disables thermal contribution
		bool NO_ITERATION = false; // flips if the grid is found to contain no ionization or attachment, and that solution only needs 1 iteration

		double calculation_time = arma::datum::nan;

		double present_eV_max; // 

	// The constructor for this is the actual execution as long as parameters were actually passed in
	public:

		//default, do nothing
		BoltzmannSolver() {

		};

		
		// on construction, perform calculation
		BoltzmannSolver(const mb::BoltzmannParameters p, const lib::Library lib) {

			this->p = p; //parameter pass in
			this->lib = lib;

			this->present_eV_max = p.initial_eV_max; // may change during calc due to remap

			if (p.use_eV_max_guess == mb::YesNo::Yes) {
				this->present_eV_max = p.EN_Td;
			}

			mb::normal_statement("Beginning MultiBolt calculation...");

			mb::debug_statement("With parameters:");
			if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
				this->p.print();
			}


			check_validity();



			if (p.T_K == 0) {
				mb::normal_statement("Gas temperature is zero, using cold-case.");
				this->COLD_CASE = true;
				this->Np = 101325.0 / 1.38E-23 / (300.0) * (760.0 / p.p_Torr);
			}
			else {
				this->Np = 101325.0 / 1.38E-23 / p.T_K * (760.0 / p.p_Torr);
			}



			this->E0 = Np * p.EN_Td * 1E-21; // electric field in V/m
			
			set_grid();
				

			timer.tic();

			execute();

			calculation_time = timer.toc();
			timer = arma::wall_clock(); // essentially, turn off the timer by blipping it

			// Now that we are finished, shovel in calculations to output
			this->out = mb::BoltzmannOutput();
			

			// exporter-relevant functions
			carve_output();
			fill_output();

			mb::normal_statement("Concluded a MultiBolt calculation.");

			
			if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
				mb::debug_statement("With output:");
				this->out.print();
			}

			return;

		}

		/*
		
		BoltzmannSolver(const bool PAUSE, const mb::BoltzmannParameters p, const lib::Library lib) {
			if (PAUSE == true){
				this->p = p; //parameter pass in
				this->lib = lib;

				this->present_eV_max = p.initial_eV_max; // may change during calc due to remap

				if (p.use_eV_max_guess == mb::YesNo::Yes) {
					this->present_eV_max = p.EN_Td;
				}

				mb::normal_statement("Paused Boltzmann Solver: Settings parsed but no solution will occur.");

				mb::debug_statement("With parameters:");
				if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
					this->p.print();
				}


				check_validity();



				if (p.T_K == 0) {
					mb::normal_statement("Gas temperature is zero, using cold-case.");
					this->COLD_CASE = true;
					this->Np = 101325.0 / 1.38E-23 / (300.0) * (760.0 / p.p_Torr);
				}
				else {
					this->Np = 101325.0 / 1.38E-23 / p.T_K * (760.0 / p.p_Torr);
				}



				this->E0 = Np * p.EN_Td * 1E-21; // electric field in V/m

				set_grid();
			}	
			else {
				mb::normal_statement("Paused Boltzmann Solver: Nothing will happen.");
			}
		};


		*/


	private:

		void execute();

		void check_validity();

		void carve_output(); // output struct instantiation, sizing
		void fill_output(); // output struct filling

		void solve_f0_SST();
		void gov_eq_f0_SST(int ell, arma::SpMat<double>& A);

		void solve_f0_HD();
		void gov_eq_f0_HD(int ell, arma::SpMat<double>& A);

		void solve_f1L();
		void gov_eq_f1L(int ell, arma::SpMat<double>& A, arma::Col<double>& b);

		void solve_f1T();
		void gov_eq_f1T(int ell, arma::SpMat<double>& A, arma::Col<double>& b);

		void solve_f2L();
		void gov_eq_f2L(int ell, arma::SpMat<double>& A, arma::colvec& b);

		void solve_f2T();
		void gov_eq_f2T(int ell, arma::SpMat<double>& A, arma::colvec& b);


		
		void C_ela_isotropic_cold(int ell, arma::SpMat<double>& A); 
		void C_ela_isotropic_thermal(int ell, arma::SpMat<double>& A);
		void C_ela_Lorentz_anisotropic(int ell, arma::SpMat<double>& A);

		void C_exc(int ell, arma::SpMat<double>& A);
		void C_att(int ell, arma::SpMat<double>& A);
		void C_iz(int ell, arma::SpMat<double>& A);
		void C_sup(int ell, arma::SpMat<double>& A);
		void full_collision_operator(int ell, arma::SpMat<double>& A);


		
		


		void set_grid();

		

		arma::colvec calculate_s_Te_eff();

		double calculate_rate(std::shared_ptr<lib::AbstractXsec> x);
		double calculate_total_rate(const lib::CollisionCode& type);

		double calculate_total_S(const arma::colvec& f, const lib::CollisionCode& type); // more generalized form of the above, useful for higher order schemes

		double calculate_growth(std::shared_ptr<lib::AbstractXsec> x);
		double calculate_total_growth(const lib::CollisionCode& type);

		double calculate_k_iz_eff_N();

		double calculate_W_f0();
		double calculate_W_BULK();

		double calculate_muN_f0();
		double calculate_muN_BULK();

		double calculate_avg_en();

		double calculate_alpha_eff_N();

		double calculate_DFTN();
		double calculate_DTN_BULK();

		double calculate_DFLN();
		double calculate_DLN_BULK();

		double calculate_DN_f0();
		double calculate_DN_f0_nu();


		double calculate_DT_mu();

		double calculate_DL_mu();

		double calculate_D_mu();

		

		double species_total_inelastic(const int gidx, int k, const std::shared_ptr<lib::Species> spec);
		double species_total_elastic(const int gidx, int k, const std::shared_ptr<lib::Species> spec);
		arma::colvec species_total_inelastic(const int gidx, arma::uvec k, const std::shared_ptr<lib::Species> spec);



		mb::RemapChoice check_grid(); // checking related to energy remap
		void lower_grid();
		void raise_grid_little();
		void raise_grid_lots();

		void check_single_ela_or_eff();


		public:
			arma::colvec calculate_MaxwellBoltzmann_EEDF(); // a courtesy: the limit case of the EEDF with no electron-neutral collisions in hot temperature
		// above is not applicable for cold case

			double check_f0_normalization();
		
	public:

		arma::colvec calculate_df_dt(const double ne, const double EN_Td, const arma::colvec& f_prev, const double k_iz_eff_N);

		// return grid-based du depending on k
		double Du_at_k(int k, int gkey) {
		
			

			// forward-facing Du, should be fine for the first-order terms
			double Du = 0;
			if (k == 0) {
				
				if (gkey == g.EVEN) {
					Du = g.ue(1) - g.ue(0);
				}
				else {
					Du = g.uo(1) - g.uo(0);
				}
				
			}
			else {
				if (gkey == g.EVEN) {
					Du = g.ue(k) - g.ue(k - 1);
				}
				else {
					Du = g.uo(k) - g.uo(k - 1);

				}
				
			}

			return Du;
		}

		mb::BoltzmannOutput get_output()
		{
			return out;
		}
		mb::BoltzmannParameters get_parameters() {
			return p;
		}

		arma::colvec get_x_f() { // useful for time-dependent solvers
			return x_f;
		}

		arma::sp_mat get_scattering_matrix() { // useful for time-dependent solvers
			return A_scattering;
		}
};



