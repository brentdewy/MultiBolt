// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// ionization operator
// cooling effect - depending on sharing, may insert large electron content to head
// hot electrons tend to be inserted midway and at low eV
void mb::BoltzmannSolver::C_iz(int ell, arma::SpMat<double>& A) {



	for (auto& spec: lib.allspecies) {

		double frac = spec->frac();

		if (mb::doubles_are_same(frac, 0)) {
			continue;
		}


		for (auto& iz : spec->iz) {

			mb::debug_statement("Applying C_iz[" + std::to_string(ell) + "] with process " + iz->process() + "in species " + spec->name());

			double u = 0;
			double totalICS = 0;

			// step in grid assosciated with ionization energy
			int dk_iz = round((iz->eV_thresh()) * mb::QE / g.Du);


			for (arma::sword k = 0; k < p.Nu; ++k) {

				// scattering-out term for the primary electron

				double FRAC_PRIMARY = 1.0 / (1.0 - p.sharing);
				int dk_PRIMARY = round(FRAC_PRIMARY * k) + dk_iz;
				// in case of one-takes-all, all energy goes to the primary and the secondary generates at "zero" instead
				
				// scattering-out term for the secondary electron
				// currently assumes equal-sharing
				double FRAC_SECONDARY = 1.0 / p.sharing;
				int dk_SECONDARY = round((FRAC_SECONDARY) * k) + dk_iz;


				
				// electron scatters-in to distribution at Zp*u + uk
				if(dk_PRIMARY < p.Nu && dk_PRIMARY >= 0) {
					if (is_even_or_zero(ell)) {
						u = g.ue(dk_PRIMARY);
						totalICS = iz->eval_at_k(g.EVEN, dk_PRIMARY);
					}
					else {
						u = g.uo(dk_PRIMARY);
						totalICS = iz->eval_at_k(g.ODD, dk_PRIMARY);
					}

					// f0(k + dk_iz)
					A.at((p.Nu * ell) + k, (p.Nu * ell) + dk_PRIMARY) +=
						 frac * (FRAC_PRIMARY) * u * Np * p.ionization_scattering.scattering_integral(u / mb::QE, ell) * totalICS;
					
					
					
				}
				
				

				// limit case for one-takes-all
				if (doubles_are_same(p.sharing, 0)) {
					if (is_even_or_zero(ell)) {
						u = g.ue(k);
						totalICS = iz->eval_at_k(g.EVEN, k);
					}
					else {
						u = g.uo(k);
						totalICS = iz->eval_at_k(g.ODD, k);
					}

					// electron scatters-in to distribution at 0
					// f0(0) : insert at 0, but evalute based on k
					A.at((p.Nu * ell) + 0, (p.Nu * ell) + k) +=
						 frac * u * Np * p.ionization_scattering.scattering_integral(u / mb::QE, ell) * totalICS;


				}
				else { // actual sharing
					if (dk_SECONDARY < p.Nu && dk_SECONDARY >= 0) {
						if (is_even_or_zero(ell)) {
							u = g.ue(dk_SECONDARY);
							totalICS = iz->eval_at_k(g.EVEN, dk_SECONDARY);
						}
						else {
							u = g.uo(dk_SECONDARY);
							totalICS = iz->eval_at_k(g.ODD, dk_SECONDARY);
						}

						// electron scatters-in to distribution at Zs*u + uk
						// f0(k + dk_iz)
						A.at((p.Nu * ell) + k, (p.Nu * ell) + dk_SECONDARY) +=
							 frac * (FRAC_SECONDARY)*u * Np * p.ionization_scattering.scattering_integral(u / mb::QE, ell) * totalICS;

					}
				}
				


								
				
				



				// electron scatters-out of distribution at u, becoming the "primary" electron when scattering back in
				if (is_even_or_zero(ell)) {
					u = g.ue(k);
					totalICS = iz->eval_at_k(g.EVEN, k);
				}
				else {
					u = g.uo(k);
					totalICS = iz->eval_at_k(g.ODD, k);
				}

				A.at((p.Nu * ell) + k, (p.Nu * ell) + k) +=
					- frac * u * Np * totalICS;



			}

		}
	}

}