// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// helper for effective xsecs
double mb::BoltzmannSolver::species_total_elastic(const int gidx, int k, const std::shared_ptr<lib::Species> spec) {

	double temp = 0;

	for (auto x : spec->ela) {
		temp = temp + x->eval_at_k(gidx, k);
	}

	for (auto x : spec->eff) {

		temp = temp + x->eval_at_k(gidx, k) - species_total_inelastic(gidx, k, spec);

	}
	
	return temp;
}

// helper for effective xsecs
double mb::BoltzmannSolver::species_total_inelastic(const int gidx, int k, const std::shared_ptr<lib::Species> spec) {

	double temp = 0;

	for (auto vec : { spec->att, spec->exc, spec->iz, spec->sup }) {
		for (auto x : vec) {
			temp = temp + x->eval_at_k(gidx, k);
		}
	}

	return temp;

}

// helper for effective xsecs
arma::colvec mb::BoltzmannSolver::species_total_inelastic(const int gidx, arma::uvec k, const std::shared_ptr<lib::Species> spec) {

	arma::colvec temp = arma::colvec(p.Nu, arma::fill::zeros);

	for (auto vec : { spec->att, spec->exc, spec->iz, spec->sup }) {
		for (auto x : vec) {
			temp = temp + x->eval_at_k(gidx, k);
		}
	}

	return temp;

}


// The elastic contribution to the collision operator
// isotropic, here, refers to ell=0
// Cold-part only - see other c_ela files for thermal contribution

// "cold" process - accounts for settling into equilibrium
void mb::BoltzmannSolver::C_ela_isotropic_cold(int ell, arma::SpMat<double>& A) {


	if (ell == 0) {

		for (auto spec : lib.allspecies) {
			double frac = spec->frac();

			if (mb::doubles_are_same(frac, 0)) {
				continue;
			}

			
			

			for (arma::sword k = 0; k < p.Nu; ++k) {

				

				double Du =  Du_at_k(k, g.EVEN);


				for (auto& ela : spec->ela) {

					if (k == 0) {
						mb::debug_statement("Applying C_ela[" + std::to_string(ell) + "] with process " + ela->process() + " in species " + spec->name());

					}
					
					
					// Uses forward difference
					

					double u = 0;
					double elasticMT_ICS = 0;


					// for ell=0, anisotropic scattering effects are all captured by the MT Xsec

					// f0(k)
					u = g.ue(k);
					elasticMT_ICS = ela->eval_at_k(g.EVEN, k);

					A.at((p.Nu * ell) + k, (p.Nu * ell) + k) +=
						  -1 * frac * 2.0 * (ela->Mratio()) / Du * (u * u) * Np * elasticMT_ICS;

					if (k + 1 < p.Nu) {
						// f0(k + 1)
						u = g.ue(k + 1);
						elasticMT_ICS = ela->eval_at_k(g.EVEN, k + 1);

						A.at((p.Nu * ell) + k, (p.Nu * ell) + k + 1) +=
							 frac * 2.0 * (ela->Mratio()) / Du * (u * u) * Np * elasticMT_ICS;
					}

				}

		
				
				// use for eff instead
				for (auto& eff : spec->eff) {

					if (k == 0) {
						mb::debug_statement("Applying C_ela[" + std::to_string(ell) + "] (effective-handling) with process " + eff->process() + " in species " + spec->name());
					}
					
					double u = 0;
					double elasticMT_ICS = 0;


					u = g.ue(k);
					elasticMT_ICS = eff->eval_at_k(g.EVEN, k) -  species_total_inelastic(g.EVEN, k, spec);

					// f0(k)
					A.at((p.Nu * ell) + k, (p.Nu * ell) + k) +=
						-1.0 * frac * 2.0 * (eff->Mratio()) / g.Du * (u * u) * Np * elasticMT_ICS;


					if (k + 1 < p.Nu) {
						// f0(k + 1)
						u = g.ue(k + 1);
						elasticMT_ICS = eff->eval_at_k(g.EVEN, k+1) - species_total_inelastic(g.EVEN, k + 1, spec);

						A.at((p.Nu * ell) + k, (p.Nu * ell) + k + 1) +=
							 frac * 2.0 * (eff->Mratio()) / g.Du * (u * u) * Np *  elasticMT_ICS;
					}
				}
			}
		}
	}
	
}

