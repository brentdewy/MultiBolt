// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// superelastic operator
// current implementation assumes isotropic scattering
void mb::BoltzmannSolver::C_sup(int ell, arma::SpMat<double>& A) {


	// superelastic processes have opposite sign convention to excitation
	// they heat the eedf rather than cool it

	for (auto& spec : lib.allspecies) {

		double frac = spec->frac();

		if (doubles_are_same(0, frac)) {
			continue;
		}
		
		for (auto& sup : spec->sup) {

			mb::debug_statement("Applying C_sup[" + std::to_string(ell) + "] with process " + sup->process() + " in species " + spec->name());


			// index to inject electron at
			int dk_exc = round((sup->parent_eV_thresh()) * mb::QE / g.Du); // is actually retrieving the eV_thresh of the parent in this case

			for (arma::sword k = 0; k < p.Nu; ++k) { // row #

				double u = 0;
				double totalICS = 0;

				// electron scatters-out of distribution at u - uk
				if (k - dk_exc < p.Nu && k - dk_exc >= 0) {
					// f0(k - dk_exc)

					if (is_even_or_zero(ell)) {
						u = g.ue(k - dk_exc);
						totalICS = sup->eval_at_k(g.EVEN, k - dk_exc); // <- totalICS
					}
					else {
						u = g.uo(k - dk_exc);
						totalICS = sup->eval_at_k(g.ODD, k - dk_exc); // <- totalICS
					}

					A.at((ell * p.Nu) + k, (ell * p.Nu) + k - dk_exc) +=
						 - frac * u * Np * p.superelastic_scattering.scattering_integral(u / mb::QE, ell) * totalICS;
				}



				// scattering-in portion
				if (is_even_or_zero(ell)) {
					u = g.ue(k);
					totalICS = sup->eval_at_k(g.EVEN, k);
				}
				else {
					u = g.uo(k);
					totalICS = sup->eval_at_k(g.ODD, k);
				}

				// electron scatters-in to distribution at u
				A.at((ell * p.Nu) + k, (ell * p.Nu) + k) +=
					frac * u * Np * totalICS;



			}
		}
	}
}
