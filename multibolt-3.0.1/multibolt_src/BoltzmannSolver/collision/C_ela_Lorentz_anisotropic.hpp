// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// Elastic collision operator applying the Lorentz approximation (angular change allowed, but assume zero energy loss as me << M)
// anisotropic, here, for all ell > 0
// "hot" process, due to no energy transfer (electrons only become hotter due to these terms while E exists)
void mb::BoltzmannSolver::C_ela_Lorentz_anisotropic(int ell, arma::SpMat<double>& A) {

	// Acts solely on the submats for ell != 0

	if (ell > 0) {

		for (auto& spec : lib.allspecies) {


			double frac = spec->frac();
			if (mb::doubles_are_same(frac, 0)) {
				continue;
			}


			for (auto& ela : spec->ela) {

				mb::debug_statement("Applying C_ela[" + std::to_string(ell) + "] with process " + ela->process() + " in species " + spec->name());


				for (arma::sword k = 0; k < p.Nu; ++k) {

					double u = 0;
					double elasticMT_ICS = 0;
					double totalICS = 0;

					if (is_even_or_zero(ell)) {
						u = g.ue(k);
						elasticMT_ICS = ela->eval_at_k(g.EVEN, k);
					}
					else {
						u = g.uo(k);
						elasticMT_ICS = ela->eval_at_k(g.ODD, k);
					}
					totalICS = p.elastic_scattering.elasticTotal_from_elasticMT(u / mb::QE, elasticMT_ICS);
					

					// for anisotropic scattering, this term actually requires the total xsec rather than the MT
					A.at((ell * p.Nu) + k, (ell * p.Nu) + k) +=
						frac * u * Np * (p.elastic_scattering.scattering_integral(u / mb::QE, ell) - 1) * totalICS;


				}

			}


			for (auto& eff : spec->eff) {

				mb::debug_statement("Applying C_ela[" + std::to_string(ell) + "] with process " + eff->process() + " in species " + spec->name());



				for (arma::sword k = 0; k < p.Nu; ++k) {
					double u = 0;
					double elasticMT_ICS = 0;
					double totalICS = 0;

					if (is_even_or_zero(ell)) {
						u = g.ue(k);
						elasticMT_ICS = eff->eval_at_k(g.EVEN, k)  - species_total_inelastic(g.EVEN, k, spec);
					}
					else {
						u = g.uo(k);
						elasticMT_ICS = eff->eval_at_k(g.ODD, k) - species_total_inelastic(g.ODD, k, spec);
					}
					totalICS = p.elastic_scattering.elasticTotal_from_elasticMT(u / mb::QE, elasticMT_ICS);

					A.at((ell * p.Nu) + k, (ell * p.Nu) + k) += // using Pell minus 1
						 frac * u * Np * (p.elastic_scattering.scattering_integral(u / mb::QE, ell) - 1.0) * totalICS;


				}
			}
		}
	}

}

