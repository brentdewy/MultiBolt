// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// Excitation operator
// strong cooling effect, tends to insert electrons near the origin
void mb::BoltzmannSolver::C_exc(int ell, arma::SpMat<double>& A) {

	for (auto& spec : lib.allspecies) {

		double frac = spec->frac();

		if (mb::doubles_are_same(frac, 0)) {
			continue;
		}

		for (auto& exc : spec->exc) {

			mb::debug_statement("Applying C_exc[" + std::to_string(ell) + "] with process " + exc->process() + " in species " + spec->name());


			// electron scatters-out of the distribution at u + uk

			int dk_exc = round((exc->eV_thresh()) * mb::QE / g.Du);

			

			for (arma::sword k = 0; k < p.Nu; ++k) { // row #

				double u = 0;
				double totalICS = 0;
				double UINT = 0;

				// electron scatters-out at u + ek
				if (k + dk_exc < p.Nu && k + dk_exc >= 0) {
					// f0(k + dk_exc)

					if (is_even_or_zero(ell)) {
						UINT = g.ue(k);
						u = g.ue(k + dk_exc);
						totalICS = exc->eval_at_k(g.EVEN, k + dk_exc); // <- totalICS
					}
					else {
						UINT = g.uo(k);
						u = g.uo(k + dk_exc);
						totalICS = exc->eval_at_k(g.ODD, k + dk_exc); // <- totalICS
					}

					A.at((ell * p.Nu) + k, (ell * p.Nu) + k + dk_exc) +=
						+ frac * u * Np * p.excitation_scattering.scattering_integral(u / mb::QE, ell) * totalICS;
				}




				// electron scatters-in to the distribution at u

				if (is_even_or_zero(ell)) {
					u = g.ue(k);
					totalICS = exc->eval_at_k(g.EVEN, k);
				}
				else {
					u = g.uo(k);
					totalICS = exc->eval_at_k(g.ODD, k);
				}

				A.at((ell * p.Nu) + k, (ell * p.Nu) + k) +=
					- frac * u * Np * totalICS;
				
			}

		}
	}

}
