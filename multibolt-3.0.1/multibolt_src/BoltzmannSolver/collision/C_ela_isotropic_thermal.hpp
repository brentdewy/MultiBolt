// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// the elastic contribution to the collision operator
// using the small-energy-change inclusive form for ell = 0 
// centering on thermal energy
// currently assumes isotropic scattering

// Thermal elastic contribution tends towards Maxwellian, can be relevant for high temperatures and low E/N (Td < 1)
// Of note, the normalization of the EEDF may be prone to failing if this term is not included for low fields

// note: thermal effects tend to heat electrons - collisions pass some thermal energy to electrons
void mb::BoltzmannSolver::C_ela_isotropic_thermal(int ell, arma::SpMat<double>& A) {




	// this expression is relying on the product rule expansion
	// term 1: 2 * u * sigma * df_du
	// term 2: u^2 * dsigma_du * df_du
	// term 3: u^2 * sigma * d2f_du2


	if (ell == 0 && !COLD_CASE) {

		// todo: add SIGN abstraction


		for (auto& spec : lib.allspecies) {

			double frac = spec->frac();
			if (mb::doubles_are_same(frac, 0)) {
				continue;
			}
			

			for (auto& ela : spec->ela) {

				mb::debug_statement("Applying C_ela(thermal)[" + std::to_string(ell) + "] with process " + ela->process() + "in species " + spec->name());

				arma::colvec elasticMT_ICS_vec(p.Nu, arma::fill::zeros);
				for (int i = 0; i < p.Nu; ++i) {
					elasticMT_ICS_vec(i) = ela->eval_at_k(g.EVEN, i);
				}



				arma::colvec d_elasticMT_du_vec(p.Nu, arma::fill::zeros);
				d_elasticMT_du_vec(g.t_idx) = (elasticMT_ICS_vec(g.t_idx) - (elasticMT_ICS_vec(g.idx_t))) / 2.0 / g.Du;
				d_elasticMT_du_vec(0) = (elasticMT_ICS_vec(1) - elasticMT_ICS_vec(0)) / g.Du;
				d_elasticMT_du_vec(p.Nu - 1) = (elasticMT_ICS_vec(p.Nu-1) - elasticMT_ICS_vec(p.Nu - 2)) / g.Du;


				


				for (arma::sword k = 0; k < p.Nu; ++k) {

				

					double FACTOR = frac * 2.0 * (ela->Mratio()) * Np * (mb::KB * p.T_K);

					// apply first derivatives kernal
					arma::colvec coeffsForward = { -3.0 / 2.0, 2.0, -1.0 / 2.0 };// worked!
					coeffsForward = coeffsForward * -1;

					for (int i = 0; i < coeffsForward.n_elem; ++i) {

						double c = coeffsForward(i);
						int idx = k + i;

						if (idx >= 0 && idx < p.Nu) {

							double u = g.ue(idx);
							double elasticMT_ICS = elasticMT_ICS_vec(idx);
							double d_DCS_du = d_elasticMT_du_vec(idx);

							A(k, idx) += (c / g.Du) * FACTOR * (2 * u) * elasticMT_ICS
								+ (c / g.Du) * FACTOR * (u * u) * d_DCS_du 
									+ (c / g.Du) * FACTOR * (u * u) * elasticMT_ICS;
							
						}
					}



					// apply second derivatives kernal
					arma::colvec coeffs = { -1.0 / 560, 8.0 / 315.0, -1.0 / 5.0, 8.0 / 5.0, -205.0 / 72.0, 8.0 / 5.0, -1.0 / 5.0, 8.0 / 315.0, -1.0 / 560.0 };

					for (int i = 0; i < coeffs.n_elem; ++i) {
						double c = coeffs(i);
						int idx = k + i - (coeffs.n_elem - 1) / 2;

						

						if (idx >= 0 && idx < p.Nu) {
							double u = g.ue(idx);
							double elasticMT_ICS = elasticMT_ICS_vec(idx);
							double d_DCS_du = d_elasticMT_du_vec(idx);

							A(k, idx) +=  (c / g.Du / g.Du) * FACTOR * (u * u) * elasticMT_ICS;

						}

					}
				}
			}

			// todo: needs update
			// apply for effective case instead
			for (auto& eff : spec->eff) {


				mb::debug_statement("Applying C_ela(thermal)[" + std::to_string(ell) + "] with process " + eff->process() + "in species " + spec->name());

				arma::colvec elasticMT_ICS_vec(p.Nu, arma::fill::zeros);
				for (int i = 0; i < p.Nu; ++i) {
					elasticMT_ICS_vec(i) = eff->eval_at_k(g.EVEN, i) - species_total_inelastic(g.EVEN, i, spec);
				}


				arma::colvec d_elasticMT_du_vec(p.Nu, arma::fill::zeros);
				d_elasticMT_du_vec(g.t_idx) = (elasticMT_ICS_vec(g.t_idx) - (elasticMT_ICS_vec(g.idx_t))) / 2.0 / g.Du;
				d_elasticMT_du_vec(0) = (elasticMT_ICS_vec(1) - elasticMT_ICS_vec(0)) / g.Du;
				d_elasticMT_du_vec(p.Nu - 1) = (elasticMT_ICS_vec(p.Nu - 1) - elasticMT_ICS_vec(p.Nu - 2)) / g.Du;



				for (arma::sword k = 0; k < p.Nu; ++k) {



					double FACTOR = frac * 2.0 * (eff->Mratio()) * Np * (mb::KB * p.T_K);

					// apply first derivatives kernal
					arma::colvec coeffsForward = { -3.0 / 2.0, 2.0, -1.0 / 2.0 };// worked!
					coeffsForward = coeffsForward * -1;

					for (int i = 0; i < coeffsForward.n_elem; ++i) {

						double c = coeffsForward(i);
						int idx = k + i;

						if (idx >= 0 && idx < p.Nu) {

							double u = g.ue(idx);
							double elasticMT_ICS = elasticMT_ICS_vec(idx);
							double d_DCS_du = d_elasticMT_du_vec(idx);

							A(k, idx) += (c / g.Du) * FACTOR * (2 * u) * elasticMT_ICS
								+ (c / g.Du) * FACTOR * (u * u) * d_DCS_du
									+ (c / g.Du) * FACTOR * (u * u) * elasticMT_ICS;

						}
					}



					// apply second derivatives kernal
					arma::colvec coeffs = { -1.0 / 560, 8.0 / 315.0, -1.0 / 5.0, 8.0 / 5.0, -205.0 / 72.0, 8.0 / 5.0, -1.0 / 5.0, 8.0 / 315.0, -1.0 / 560.0 };

					for (int i = 0; i < coeffs.n_elem; ++i) {
						double c = coeffs(i);
						int idx = k + i - (coeffs.n_elem - 1) / 2;



						if (idx >= 0 && idx < p.Nu) {
							double u = g.ue(idx);
							double elasticMT_ICS = elasticMT_ICS_vec(idx);
							double d_DCS_du = d_elasticMT_du_vec(idx);

							A(k, idx) += (c / g.Du / g.Du) * FACTOR * (u * u) * elasticMT_ICS;

						}

					}
				}


			}

		}
	}
}

