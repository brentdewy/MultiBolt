// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 
#pragma once

#include "multibolt"

// Attachment contribution to collision operator
// Because attachment tends to be a low-eV process, low-energy electrons will tend to be removed from the system
void mb::BoltzmannSolver::C_att(int ell, arma::SpMat<double>& A) {



	for (auto& spec : lib.allspecies) {


		double frac = spec->frac();

		if (mb::doubles_are_same(frac, 0)) {
			continue;
		}


		for (auto& att : spec->att) {

			mb::debug_statement("Applying C_att[" + std::to_string(ell) + "] with process " + att->process() + " in species " + spec->name());

			for (arma::sword k = 0; k < p.Nu; ++k) {

				

				double u = 0;
				double totalICS = 0;

				if (mb::is_even_or_zero(ell)) {// if ell is even or zero
					u = g.ue(k);
					totalICS = att->eval_at_k(g.EVEN, k);
				}
				else { // if ell is odd
					u = g.uo(k);
					totalICS = att->eval_at_k(g.ODD, k);
				}

				// electron scatters-out at u, and vanishes (non-conservative)
				A.at((ell * p.Nu) + k, (ell * p.Nu) + k) +=
					- frac * u * Np * totalICS;

			}


		}

	}
}

