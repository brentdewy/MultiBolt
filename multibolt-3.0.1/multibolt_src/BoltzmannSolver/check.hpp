// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


double mb::BoltzmannSolver::check_f0_normalization() {
	return arma::accu(trapz(g.ue / mb::QE, x_f(g.ell0_idx) % sqrt(g.ue / mb::QE)));
}




// Check if the grid maximum deserves to be adjusted
// to lower u_max, raise it, or leave it alone
mb::RemapChoice mb::BoltzmannSolver::check_grid() {

	arma::uvec chopped = arma::find(g.ell0_idx >= p.Nu * 0.01 && g.ell0_idx < p.Nu * 0.95);

	arma::uvec fbody_idx = arma::find_unique(x_f(g.ell0_idx));
	fbody_idx = arma::intersect(chopped, fbody_idx);

	arma::colvec fbody = x_f(fbody_idx);


	double orderspan = arma::max(arma::log10(fbody)) - arma::min(arma::log10(fbody));

	if (orderspan > p.remap_target_order_span + 1) { //  too many orders are covered
		return mb::RemapChoice::LowerGrid;
	}
	else if (orderspan < p.remap_target_order_span / 2.0) { //  too few orders are covered, by a longshot
		return mb::RemapChoice::RaiseGridLots;
	}
	else if (orderspan < p.remap_target_order_span - 1) { //  too few orders are covered, but only by a little bit
		return mb::RemapChoice::RaiseGridLittle;
	}


	return mb::RemapChoice::KeepGrid;
}


void mb::BoltzmannSolver::lower_grid() {

	//todo: turned into spagetti code, this could be merged
	arma::uvec chopped = arma::find(g.ell0_idx >= p.Nu * 0.01 && g.ell0_idx < p.Nu * 0.95);

	arma::uvec fbody_idx = arma::find_unique(x_f(g.ell0_idx));
	fbody_idx = arma::intersect(chopped, fbody_idx);

	arma::colvec fbody = x_f(fbody_idx);

	double target_order = arma::max(arma::log10(fbody)) - p.remap_target_order_span;
	double target_tail = pow(10, target_order);




	arma::uvec pt = arma::find(fbody < target_tail, 1, "first");


	present_eV_max = arma::accu(g.ue(fbody_idx(pt))) / mb::QE;


	mb::debug_statement("Grid roof was lowered to " + mb::mb_format(present_eV_max) + " eV.");

	// now reset grid
	set_grid();


}

void mb::BoltzmannSolver::raise_grid_little() {

	present_eV_max = present_eV_max * 1.1; // raise by 10%

	mb::debug_statement("Grid roof was raised to " + mb::mb_format(present_eV_max) + " eV.");

	// now reset grid 
	set_grid();

}

void mb::BoltzmannSolver::raise_grid_lots() {

	present_eV_max = present_eV_max * 2; // raise by 100%

	mb::debug_statement("Grid roof was raised to " + mb::mb_format(present_eV_max) + " eV.");

	// now reset grid 
	set_grid();

}

// todo: move this elsewhere
void mb::BoltzmannSolver::check_validity() {

	double fracsum = 0;

	// no gas should have a negative fraction
	for (auto& spec : lib.allspecies) {

		fracsum += spec->frac();

		if (spec->frac() < 0) {
			throw_err_statement("Cannot solve: Species must have positive fractional presence.");
		}

		for (auto& x : spec->allcollisions) {
			if (spec->frac() < 0) {
				throw_err_statement("Cannot solve: Xsecs must have positive scales.");
			}
		}

	}
	
	// fractional portions of gases must add up to 1.0
	if (p.sharing > 0.5 || p.sharing < 0) {
		throw_err_statement("Cannot solve: Energy-sharing parameter (delta) must be in range [0, 0.5]");
	}

	// fractional portions of gases must add up to 1.0
	if (abs(fracsum - 1.0) > 1e-5 ) {
		throw_err_statement("Cannot solve: Fractional sum must add to 1.0");
	}
	


	if (p.N_terms % 2 != 0) {
		throw_err_statement("Cannot solve: N_terms must be an even number.");
	}



	if (p.N_terms < 2) {
		throw_err_statement("Cannot solve: N_terms must be greater than or equal to 2.");
	}


	// Make sure you're not doubling up on ela or eff per-species
	for (auto& spec : lib.allspecies) {
		if (spec->n_ela() > 0 && spec->n_eff() > 0) {
			throw_err_statement("Cannot solve: You may only use exactly one elastic momentum transfer for effective Xsec per-species.");
		}

		if (spec->n_eff() > 1) {
			throw_err_statement("Cannot solve: Assigning more than one effective Xsec to a species is disallowed.");
		}
	}

}