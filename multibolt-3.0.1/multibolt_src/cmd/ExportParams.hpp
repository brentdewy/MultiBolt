// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

using std::string;


// This is just a helper for accepting command line arguments
class ExportParams {
public:

	string export_location = mb::UNALLOCATED_STRING;
	string export_name = mb::UNALLOCATED_STRING;
	bool no_export = false;

	bool export_xsecs = false; // tends to make large files, disable by default

};