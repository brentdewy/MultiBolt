// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"


// this is just a helper to do command line argument stuff
class SweepParams {

public:
	
	double start = arma::datum::nan;
	double stop = arma::datum::nan;
	int points = arma::datum::nan;

	mb::sweep_option sweep_option = mb::sweep_option::EN_Td;
	mb::sweep_style sweep_style = mb::sweep_style::lin;

	std::vector<double> defval = {};

};