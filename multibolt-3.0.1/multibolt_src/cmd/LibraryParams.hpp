// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

// helper object for executable
class LibraryParams {
public:

	std::vector<std::string> LXCat_Xsec_fids = {};

	
	std::vector<std::string> species = {};  // goes together
	std::vector<double> fracs = {};    // goes together


	std::vector<std::string> processes = {};  // goes together
	std::vector<double> scales = {};    // goes together

	bool KEEP_ALL_SPECIES = false;

};