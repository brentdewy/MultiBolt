// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"



// Numerical constants

static const double HARTREE = 27.211386; // Hartree energy (eV)

static const int ANGULAR_N = 2000; // grid resolution between 0,pi for solid angle integrals (slow-eval)

static const double PI = 3.14159265358979323846;
static const double KB = 1.38064852e-23;		// Boltzmann constant (J/K)
static const double QE = 1.60217663e-19;		// Elementary charge (C)
static const double ME = 9.10938370e-31;		// Mass of the electron (kg)
static const double AMU = 1.66053906e-27;		// Atomic mass unit (AMU, in kg)

static const std::string UNALLOCATED_STRING = "UNALLOCATED"; // this is for printing, placeholder for 
static const int UNALLOCATED_INTEGER = arma::datum::nan; // This is for size indicative integers when size is not meaningful in this case
static const double UNALLOCATED_DOUBLE = arma::datum::nan;




static const std::string FORMAT_CODE = "%1.5e";


static const std::string DEFAULT_EXPORT_PATH = "/Exported_MultiBolt_Data/";

static const double DEFAULT_NP = 101325.0 / 1.38E-23 / 300.0 * (200.0 / 760.0);//6.4407e+24;













const std::map<mb::ModelCode, std::string> model_map = { {mb::ModelCode::HD, "HD"},
															{mb::ModelCode::HDGE, "HD+GE"},
															{mb::ModelCode::SST, "SST"} };

const std::map<mb::YesNo, std::string> yesno_map = { {mb::YesNo::Yes, "Yes"},
														{mb::YesNo::No, "No"} };


const std::map<mb::ScatteringCode, std::string> scattering_map = { {mb::ScatteringCode::Isotropic, "Isotropic"},
																		{mb::ScatteringCode::IdealForward, "IdealForward"},
																		{mb::ScatteringCode::ScreenedCoulomb, "ScreenedCoulomb"} };

arma::superlu_opts multibolt_superlu_opts() {

	// See armadillo documentation for details
	// the following are generally suitable

	arma::superlu_opts opts;
	opts.equilibrate = true;
	opts.refine = arma::superlu_opts::REF_EXTRA;
	opts.allow_ugly = true; // absolutely required: A matrix may have a large condition number based on the size of Xsecs

	return opts;
}









	