// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


// See: Lucas J and Salee H T 1975 J Phys. D: Appl. Phys. 8 640-50
// Attachment modification: Ness K F and Robson R E 1986 Phys. REv. A. 34 2185
lib::Library get_LucasSalee_Library(double F, double a, double p, double INVMRATIO = 1000, double EV_THRESH = 15.6) {

	lib::Library lib = lib::Library();
	lib.add_blank_species("LucasSalee_neutral");
	lib.add_blank_species("LucasSalee_excitedstate");
	lib.add_blank_species("LucasSalee_posion");
	lib.add_blank_species("LucasSalee_negion");

	// Set up LS elastic Xsec
	std::function<double(double)> foo1 = [](double eV) {
		return 4.0 / sqrt(eV) * 1e-20;
	};
	lib::FunctionalXsec LucasSalee_elastic = lib::FunctionalXsec(foo1, lib::CollisionCode::elastic);
	LucasSalee_elastic.Mratio(1.0 / INVMRATIO);
	LucasSalee_elastic.reactant("LucasSalee_neutral");
	LucasSalee_elastic.product("LucasSalee_neutral");
	LucasSalee_elastic.process("E + LucasSalee_neutral -> E + LucasSalee_neutral, Elastic");



	// Set up LS excitation Xsec
	std::function<double(double)> foo2 = [F, EV_THRESH](double eV) {
		if (eV < EV_THRESH) {
			return 0.0;
		}
		return 0.1 * (1 - F) * (eV - EV_THRESH) * 1e-20;
	};
	lib::FunctionalXsec LucasSalee_excitation = lib::FunctionalXsec(foo2, lib::CollisionCode::excitation);
	LucasSalee_excitation.eV_thresh(EV_THRESH);
	LucasSalee_excitation.reactant("LucasSalee_neutral");
	LucasSalee_excitation.product("LucasSalee_excitedstate");
	LucasSalee_excitation.process("E + LucasSalee_neutral -> E + LucasSalee_excitedstate, Excitation");




	// Set up LS ionization Xsec
	std::function<double(double)> foo3 = [F, EV_THRESH](double eV) {
		if (eV < EV_THRESH) {
			return 0.0;
		}
		return 0.1 * F * (eV - EV_THRESH) * 1e-20;
	};
	lib::FunctionalXsec LucasSalee_ionization = lib::FunctionalXsec(foo3, lib::CollisionCode::ionization);
	LucasSalee_ionization.eV_thresh(EV_THRESH);
	LucasSalee_ionization.reactant("LucasSalee_neutral");
	LucasSalee_ionization.product("LucasSalee_posion");
	LucasSalee_ionization.process("E + LucasSalee_neutral -> E + E + LucasSalee_posion, Ionization");



	// Set up LS attachment Xsec
	std::function<double(double)> foo4 = [a, p, EV_THRESH](double eV) {
		return a * pow(eV, p) * 1e-20;
	};
	lib::FunctionalXsec LucasSalee_attachment = lib::FunctionalXsec(foo4, lib::CollisionCode::attachment);
	LucasSalee_attachment.reactant("LucasSalee_neutral");
	LucasSalee_attachment.product("LucasSalee_negion");
	LucasSalee_attachment.process("E + LucasSalee_neutral -> LucasSalee_negion, Attachment");


	lib.add_Xsec(std::make_shared<lib::FunctionalXsec>(LucasSalee_elastic));
	lib.add_Xsec(std::make_shared<lib::FunctionalXsec>(LucasSalee_excitation));
	lib.add_Xsec(std::make_shared<lib::FunctionalXsec>(LucasSalee_ionization));
	lib.add_Xsec(std::make_shared<lib::FunctionalXsec>(LucasSalee_attachment));

	lib.assign_fracs_by_names("LucasSalee_neutral", 1.0);


	return lib;
}

