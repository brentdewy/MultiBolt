// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

// several scattering objects which handle the common cases

mb::Scattering isotropic_scattering() {
	
	mb::Scattering X = mb::Scattering();

	X.name = "Isotropic";

	return X;
}

mb::Scattering forward_scattering() {

	// todo: remove when validation complete
	mb::normal_statement("Developer's note: *** WARNING *** Forward scattering is not yet validated. *** USE AT YOUR OWN RISK ***.");

	std::function<double(const double, const double)> foo_I_bar = [](const double eV, const double chi) {
		if (chi == 0) { return (1.0 / (4.0 * mb::PI)); }
		else { return 0.0; }
	};

	std::function<double(const double, const int)> foo_user_integral_solidangle_Ibar_legendrePell = [](const double eV, const int ell) {
		return 1.0;
	};

	std::function<double(const double, const double)> foo_user_elasticTotal_from_elasticMT = [](const double eV, const double elasticMT) {
		return arma::datum::nan;
	};

	mb::Scattering X = mb::Scattering(foo_I_bar, foo_user_integral_solidangle_Ibar_legendrePell, foo_user_elasticTotal_from_elasticMT);
	X.name = "IdealForward";

	return X;

}



mb::Scattering screened_coulomb_scattering(double SCREEN_EV = mb::HARTREE) {

	// todo: remove when validation complete
	mb::normal_statement("Developer's note: *** WARNING *** Screened Coulomb scattering is not yet validated. *** USE AT YOUR OWN RISK ***.");

	std::function<double(const double, const double)> foo_I_bar = [SCREEN_EV](const double eV, const double chi) {

		double XI = 4 * eV / SCREEN_EV / (1 + 4 * eV / SCREEN_EV);
		return 1.0 / (4 * mb::PI) * (1 - XI * XI) / pow((1 - XI * cos(chi)), 2);
	};

	mb::Scattering X = mb::Scattering(foo_I_bar);
	X.name = "ScreenedCoulomb(" + std::to_string(SCREEN_EV) + " eV)";

	return mb::Scattering(foo_I_bar);

}