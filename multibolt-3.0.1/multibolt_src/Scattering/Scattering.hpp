// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


#pragma once

#include <multibolt>



// A scattering object works by knowing the function for Ibar
// What's important is evaluating the solid angle integrals including Ibar and Pell
// If the user knows an analytic (fast) evaluation for the integral, you can override the
// comparatively-slow integral user the functions called "user_"
// Todo: scattering will become an object owned by the cross section in the future
class Scattering {

	private:
		bool USE_SLOW_EVAL = false;

	public:

	std::string name;


	// angular scattering treatment, accepts energy in eV and angle in radians
	std::function<double(const double, const double)> I_bar;

	// allows there to be a fast-eval method if one is known by the user
	std::function<double(const double, const int)> user_integral_solidangle_Ibar_legendrePell;
	std::function<double(const double, const double)> user_elasticTotal_from_elasticMT;


	// default case: assume using Isotropic scattering
	Scattering() {

		this->USE_SLOW_EVAL = false;

		this->I_bar = [](const double eV, const double chi) { return 1.0 / (4.0 * mb::PI); };

		this->user_integral_solidangle_Ibar_legendrePell = [](const double eV, const int ell) {
			if (ell == 0) { return 1; }
			else { return 0; }
		};

		this->user_elasticTotal_from_elasticMT = [](const double eV, const double elasticMT) { return elasticMT; };

		this->name = mb::UNALLOCATED_STRING;
	};




	// if you only pass in an I_bar, assume you can only ever do slow-evals
	Scattering(std::function<double(const double, const double)> foo_I_bar) {

		this->USE_SLOW_EVAL = true;
		this->I_bar = foo_I_bar;

		this->name = "UserDefined";

	};

	

	// user constructed passing in specific functions, you definitely want to use the fast evals as provided
	Scattering(std::function<double(const double, const double)> foo_I_bar,
		std::function<double(const double, const int)> foo_user_integral_solidangle_Ibar_legendrePell,
		std::function<double(const double, const double)> foo_user_elasticTotal_from_elasticMT) {

		this->USE_SLOW_EVAL = false;

		this->I_bar = foo_I_bar;

		this->user_integral_solidangle_Ibar_legendrePell = foo_user_integral_solidangle_Ibar_legendrePell;

		this->user_elasticTotal_from_elasticMT = foo_user_elasticTotal_from_elasticMT;
		
		this->name = "UserDefined";
	}

	// Scattering-angle-"two", evaluate for inelastic collisions
	double scattering_integral(const double eV, const int ell) {
		
		if ( this->USE_SLOW_EVAL == true) {
		
			arma::colvec angles = arma::linspace(0, mb::PI, mb::ANGULAR_N);
			arma::colvec legendreP_cosx(mb::ANGULAR_N, arma::fill::zeros);
			arma::colvec I_bar_eval(mb::ANGULAR_N, arma::fill::zeros);

			for (int i = 0; i < angles.n_elem; ++i) {
				legendreP_cosx(i) = std::legendre(ell, cos(angles(i)));
				I_bar_eval(i) = I_bar(eV, angles(i));
			}

			return 2.0 * mb::PI * arma::accu(arma::trapz(angles, legendreP_cosx % sin(angles) % I_bar_eval));

		}
		else {
			return user_integral_solidangle_Ibar_legendrePell(eV, ell);
		}
	}



	

	
	// Ibar is used to infer the shape of the total elastic cross section from the momentum transfer xsec which
	// is otherwise conserved
	double elasticTotal_from_elasticMT(const double eV, const double elasticMT) {

		if (USE_SLOW_EVAL == true) {

			arma::colvec angles = arma::linspace(0, mb::PI, mb::ANGULAR_N);
			arma::colvec I_bar_eval(mb::ANGULAR_N, arma::fill::zeros);

			for (int i = 0; i < angles.n_elem; ++i) {
				I_bar_eval(i) = I_bar(eV, angles(i));
			}
			double RATIO = 2.0 * mb::PI * arma::accu(arma::trapz(angles, (1 - cos(angles)) % sin(angles) % I_bar_eval));
			return  elasticMT / RATIO;

		}
		else {
			return user_elasticTotal_from_elasticMT(eV, elasticMT);
		}
	}



};
