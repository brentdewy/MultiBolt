// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

typedef enum class YesNo
{
	Yes = 1,
	No = 0
} YesNo;

typedef enum class ModelCode
{
	SST,
	HD,
	HDGE
} ModelCode;



typedef enum class OutputOption
{
	DEBUG,
	NORMAL,
	SILENT
} OutputOption;

typedef enum class sweep_option {

	EN_Td,
	T_K,
	p_Torr, // deprecated?
	bin_frac,
	Nu,
	N_terms,
	none   // deprecated

} sweep_option;

typedef enum class sweep_style {
	lin,
	log,
	reg,
	def,
	single // deprecated
} sweep_style;

typedef enum class RemapChoice {

	KeepGrid,
	LowerGrid,
	RaiseGridLittle,
	RaiseGridLots

} RemapChoice;


typedef enum class ScatteringCode {

	Isotropic,
	IdealForward,
	ScreenedCoulomb

} ScatteringCode;








	