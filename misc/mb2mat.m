% Draft version
% Max Flynn, Nov 2021
% Given a directory with exported MultiBolt files, turn it into a struct
% Note: this tends to be slow!


function [mb] = mb2mat(pathstr)

if ~isdir(pathstr)
    disp('Warning: not a valid path.')
    mb = []
    return
end

total_growth_names = {'alpha_N', 'eta_N'};
pergas_growth_names = {'alpha_N', 'eta_N'};

total_rate_names = {'k_att_N', ...
                                        'k_ela_N', ...
                                        'k_eff_N', ...
                                        'k_exc_N', ...
                                        'k_iz_N', ...
                                        'k_sup_N'};

pergas_rate_names = {'k_att_N', ...
                                        'k_ela_N', ...
                                        'k_eff_N', ...
                                        'k_exc_N', ...
                                        'k_iz_N', ...
                                        'k_sup_N'};

sep = "\\";

mb = struct('RateCoeffs', [], ...
      'GrowthCoeffs', [], ...
      'Distributions', [], ...
    'Energy', [], ...
    'Diffusion', [], ...
    'Mobility', [], ...
    'Velocity', [], ...
    'sweep', [], ...
     'species', [], ...
    'time', [], ...
    'eV_max', [], ...
    'sourcefile', [], ...
    'sweep_variable', []);

mbImportOptions = { 'CommentStyle', '#', 'VariableNamingRule', 'preserve'};


% figure out how many species exist
mb.species = readtable(pathstr + sep + "species_indices.txt", mbImportOptions{:}).(2);
N_spec = height(mb.species);



% figure out how many sweeps exist
mb.sweep = readtable(pathstr + sep + "sweep_indices.txt", mbImportOptions{:}).(2);
N_sweep = height(mb.sweep);

mb.RateCoeffs = struct('Total', struct(), 'PerGas', struct());
for s = total_rate_names
    
    x =  readtable(pathstr + sep + "Total" + sep + s + ".txt", mbImportOptions{:}).(2);
    mb.RateCoeffs.Total = setfield(mb.RateCoeffs.Total, s{:}, x);
   
end


 mb.RateCoeffs.PerGas = {};
for i = 1:N_spec
         mb.RateCoeffs.PerGas{i} = struct();
        
    mb.RateCoeffs.PerGas{i}.k_att_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "k_att_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.k_att_N{k} = x;
        end
        
        mb.RateCoeffs.PerGas{i}.k_ela_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "k_ela_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.k_ela_N{k} = x;
        end
        
        mb.RateCoeffs.PerGas{i}.k_eff_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "k_eff_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.k_eff_N{k} = x;
        end
         
         
       mb.RateCoeffs.PerGas{i}.k_exc_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "k_exc_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.k_exc_N{k} = x;
        end
           

        mb.RateCoeffs.PerGas{i}.k_iz_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "k_iz_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.k_iz_N{k} = x;
        end
           
    
        mb.RateCoeffs.PerGas{i}.k_sup_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "k_sup_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.k_sup_N{k} = x;
        end
 
end


mb.GrowthCoeffs = struct('Total', struct(), 'PerGas', struct());
for s = total_growth_names
    x =  readtable(pathstr + sep + "Total" + sep + s + ".txt", mbImportOptions{:}).(2);
    mb.GrowthCoeffs.Total = setfield(mb.GrowthCoeffs.Total, s{:}, x);
end

 mb.GrowthCoeffs.PerGas = {};
for i = 1:N_spec
         mb.GrowthCoeffs.PerGas{i} = struct();
        
    mb.GrowthCoeffs.PerGas{i}.alpha_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "alpha_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.GrowthCoeffs.PerGas{i}.alpha_N{k} = x;
        end
        
        mb.GrowthCoeffs.PerGas{i}.eta_N = {};
        files =  dir(fullfile(pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1), "eta_N_*.txt" ));
        for k = 1:length(files)
            thispath = pathstr + sep + "PerGas" + sep + "Gas_" + num2str(i-1) + sep +  files(k).name;
                x =  readtable(thispath, mbImportOptions{:}).(2);
                mb.RateCoeffs.PerGas{i}.eta_N{k} = x;
        end
 
end

mb.k_iz_eff_N = readtable(pathstr + sep + "k_iz_eff_N.txt", mbImportOptions{:}).(2);
mb.alpha_eff_N = readtable(pathstr + sep + "alpha_eff_N.txt", mbImportOptions{:}).(2);


    mb.Distributions = struct('EEDFs_f0', [], 'EEDFs_f1', []);    
    for i = 1 : N_sweep
        mb.Distributions.EEDFs_f0{i} = readtable(pathstr + sep + "EEDFs_f0\f0_" + num2str(i-1) + ".txt", mbImportOptions{:});
        mb.Distributions.EEDFs_f1{i} = readtable(pathstr + sep + "EEDFs_f1\f1_" + num2str(i-1) + ".txt", mbImportOptions{:});
    end
    
mb.Energy = struct('avg_en', [], 'D_mu', [], 'DT_mu', [], 'DL_mu', []);
mb.Energy.avg_en = readtable(pathstr + sep + "avg_en.txt", mbImportOptions{:}).(2);
mb.Energy.D_mu = readtable(pathstr + sep + "D_mu.txt", mbImportOptions{:}).(2);
mb.Energy.DL_mu = readtable(pathstr + sep + "DL_mu.txt", mbImportOptions{:}).(2);
mb.Energy.DT_mu = readtable(pathstr + sep + "DT_mu.txt", mbImportOptions{:}).(2);

 
 mb.Diffusion = struct('DN_f0', [], 'DN_f0_nu', [], 'DLN_FLUX', [], 'DLN_BULK', [],  'DTN_FLUX', [], 'DTN_BULK', []);
mb.Diffusion.DN_f0 = readtable(pathstr + sep + "DN_f0.txt", mbImportOptions{:}).(2);     
mb.Diffusion.DN_f0_nu = readtable(pathstr + sep + "DN_f0_nu.txt", mbImportOptions{:}).(2);    
mb.Diffusion.DLN_FLUX = readtable(pathstr + sep + "DLN_FLUX.txt", mbImportOptions{:}).(2);   
mb.Diffusion.DLN_BULK = readtable(pathstr + sep + "DLN_BULK.txt", mbImportOptions{:}).(2);
mb.Diffusion.DTN_FLUX = readtable(pathstr + sep + "DTN_FLUX.txt", mbImportOptions{:}).(2);    
mb.Diffusion.DTN_BULK = readtable(pathstr + sep + "DTN_BULK.txt", mbImportOptions{:}).(2);    

mb.Mobility = struct('muN_SST', [], 'muN_FLUX', [], 'muN_BULK', []);
mb.Mobility.muN_SST = readtable(pathstr + sep + "muN_SST.txt", mbImportOptions{:}).(2);  
mb.Mobility.muN_FLUX = readtable(pathstr + sep + "muN_FLUX.txt", mbImportOptions{:}).(2);  
mb.Mobility.muN_BULK = readtable(pathstr + sep + "muN_BULK.txt", mbImportOptions{:}).(2);  
    
mb.Velocity = struct('W_SST', [], 'W_FLUX', [], 'W_BULK', []);
mb.Velocity.W_SST = readtable(pathstr + sep + "W_SST.txt", mbImportOptions{:}).(2);  
mb.Velocity.W_FLUX = readtable(pathstr + sep + "W_FLUX.txt", mbImportOptions{:}).(2);  
mb.Velocity.W_BULK = readtable(pathstr + sep + "W_BULK.txt", mbImportOptions{:}).(2);  

mb.sourcefile = pathstr;
mb.time = readtable(pathstr + sep + "time.txt", mbImportOptions{:}).(2);
mb.eV_max =  readtable(pathstr + sep + "eV_max.txt", mbImportOptions{:}).(2);

x = readtable(pathstr + sep + "sweep_indices.txt", mbImportOptions{:});
mb.sweep_variable = x.Properties.VariableNames{2};

    

return

end