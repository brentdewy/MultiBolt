// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// This source file, when compiled, creates several analytic species and tests the program
// using the same kind of validation tables as seen in Stephens (2018)
// Compares against Reid Ramp and Lucas Salee models versus a table of known values from MCC

// these amainly function as validators for the governing equations

// specifically checks the SST governing equation

#include "multibolt"



int main() {


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();

	


	/* Develop library of analytic species -------------------------------------------------------- */

	lib::Library lib = mb::get_ReidRamp_Library();
	
	

	
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::SST;
	p.N_terms = 6; // SST tends to be well served by any number of terms

	p.Nu = 1000;	

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. 


	p.initial_eV_max = 40;

	p.EN_Td = 10;
	p.p_Torr = 760;
	p.T_K = 0;

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization

	p.elastic_scattering = mb::isotropic_scattering();
	p.excitation_scattering = mb::isotropic_scattering();
	p.ionization_scattering = mb::isotropic_scattering();
	p.superelastic_scattering = mb::isotropic_scattering();



	/* Perform Boltzmann solver calculation  */

	// instantiate
	mb::BoltzmannSolver run;
	mb::BoltzmannOutput out;

	// vectors of ouputs:
	std::vector<mb::BoltzmannOutput> LS_SST_sols = {};

	mb::normal_statement("Using following parameters as starting-point for validation:");
	p.print();



	std::vector<std::vector<double>> args = { {0, 0, 0},{ 0.25,0,0 }, { 0.5,0,0 }, { 0.75,0,0 }, { 1.0,0,0 }, {0, 5e-4, 0.5}, {0, 2e-3, -0.5}, {0, 8e-3,-1.0 } };
	p.EN_Td = 10; // static

	for (auto triplet : args) {

		double Farg = triplet[0];
		double aarg = triplet[1];
		double parg = triplet[2];

		if (Farg == 0 && aarg == 0) { // in this event, no ionization or attachment occurs
			p.iter_max = 1;
			p.iter_min = 1;
		}
		else {
			p.iter_max = 100;
			p.iter_min = 4;
		}

		lib::Library LucasSalee = mb::get_LucasSalee_Library(Farg, aarg, parg);
		mb::normal_statement("Beginning validation solution: LucasSalee, SST, E/N = 10 Td : F= " + std::to_string(Farg) + ", a=" + std::to_string(aarg) + ", p=" + std::to_string(parg));
		run = mb::BoltzmannSolver(p, LucasSalee);
		LS_SST_sols.push_back(run.get_output());
		//run.get_output().print();
	}


	// Some fake output data structures which will match the MCC comparisons



	/* ----------------------------------------------- */

	// SST model data

	// See: Dujko S, White R D, Petrovic Z Lj 2008 J. Phys. D: Appl. Phys. 41 245205
	std::vector<mb::BoltzmannOutput> Dujko_SST_LS_vec = { mb::BoltzmannOutput(), mb::BoltzmannOutput(), mb::BoltzmannOutput(), mb::BoltzmannOutput(), mb::BoltzmannOutput(), mb::BoltzmannOutput(), mb::BoltzmannOutput(), mb::BoltzmannOutput() };
	Dujko_SST_LS_vec[0].avg_en = 5.57;
	Dujko_SST_LS_vec[0].W_SST = 7.32e4;

	Dujko_SST_LS_vec[1].avg_en = 5.30;
	Dujko_SST_LS_vec[1].W_SST = 7.08e4;

	Dujko_SST_LS_vec[2].avg_en = 5.11;
	Dujko_SST_LS_vec[2].W_SST = 6.92e4;

	Dujko_SST_LS_vec[3].avg_en = 4.95;
	Dujko_SST_LS_vec[3].W_SST = 6.79e4;

	Dujko_SST_LS_vec[4].avg_en = 4.82;
	Dujko_SST_LS_vec[4].W_SST = 6.68e4;

	Dujko_SST_LS_vec[5].avg_en = 5.66;
	Dujko_SST_LS_vec[5].W_SST = 7.89e4;

	Dujko_SST_LS_vec[6].avg_en = 5.71;
	Dujko_SST_LS_vec[6].W_SST = 7.72e4;

	Dujko_SST_LS_vec[7].avg_en = 6.04;
	Dujko_SST_LS_vec[7].W_SST = 8.17e4;

	/*   ------    */
	



	for (int i = 0; i < LS_SST_sols.size(); ++i) {

		std::stringstream ss;

		double err = (LS_SST_sols[i].avg_en - Dujko_SST_LS_vec[i].avg_en) / abs(LS_SST_sols[i].avg_en);
		ss << "TEST: " << i << ": avg_en :\t\tMB: " << LS_SST_sols[i].avg_en << "\t\tMC: " << Dujko_SST_LS_vec[i].avg_en << "\t\t" << err * 100 << "%";


		err = (LS_SST_sols[i].W_SST - Dujko_SST_LS_vec[i].W_SST) / abs(LS_SST_sols[i].W_SST);
		ss << "\n";

		ss << "TEST: " << i << ": W_SST :\t\tMB: " << LS_SST_sols[i].W_SST << "\t\tMC: " << Dujko_SST_LS_vec[i].W_SST << "\t\t" << err * 100 << "%";

		std::cout << "\n" + ss.str();
		std::cout << std::endl;

	}


	return 0;
}







