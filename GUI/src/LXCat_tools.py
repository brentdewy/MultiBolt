


capital_colls = {
    "ATTACHMENT":0,
    "ELASTIC":1,
    "EFFECTIVE":2,
    "EXCITATION":3,
    "IONIZATION":4,
    "SUPERELASTIC":5
    }

normal_colls = {
    "Attachment":0,
    "Elastic":1,
    "Effective":2,
    "Excitation":3,
    "Ionization":4
    }


singleArrow = "->"
doubleArrow = "<->"

##---
def reactant_from_process(proc):
    if doubleArrow in proc:
        ls = proc.split(doubleArrow)
    elif singleArrow in proc:
        ls = proc.split(singleArrow)

    return ls[0].strip("E + ")


##---
def product_from_process(proc):
    if doubleArrow in proc:
        ls = proc.split(doubleArrow)
    elif singleArrow in proc:
        ls = proc.split(singleArrow)

    for key in normal_colls:
        if key in ls[-1]:
            newls = ls[-1].split(", {}".format(key))
            return newls[0].strip("E +").strip()


##---
def remove_duplicate_species(specs_list):
     return list(dict.fromkeys(specs_list))


##---
def find_species_in_LXCat(filename):
    specs_list = list()

    # case to read from multiple files in a row
    if isinstance(filename, list):
        for file in filename:
            specs_in_file = find_species_in_LXCat(str(file))
            specs_list.extend(specs_in_file)
        
        
        return remove_duplicate_species(specs_list)

    try:
        read = open(filename, 'r')
        read.close()

    except FileNotFoundError :
        print("Xsec file {} could not be opened.".format(filename))
        return [];

    #begin actually reading
    with open(filename, 'r') as f:

        for index, line in enumerate(f):
            for key in capital_colls:
                if key == line.strip():

                    nextline = f.readline()
                    specs = []

                    # this tends to happen with elastiscs or single-species
                    if not (singleArrow in nextline) and not (doubleArrow in nextline):
                        specs_list.append(nextline.strip())
                        break

                    # for multi-species
                    if doubleArrow in nextline:
                        specs = nextline.split(doubleArrow)
                    elif singleArrow in nextline:
                        specs = nextline.split(singleArrow)

                    
                    for i, s in enumerate(specs, start=0):
                        #print("Trying to add species: {}".format(s.strip()))
                        specs_list.append(specs[i].strip())

                    #print("Found species: {}".format(cleanspecs))

                    break

    #remove duplicate entries - yes this is faster as a set, but I'd prefer to preserve the order    
    return remove_duplicate_species(list(dict.fromkeys(specs_list)))


##---
def find_processes_in_LXCat(filename):
    proc_list = list()
    if isinstance(filename, list):
        #print("was found to be list!")
        for f in filename:
            #print("f is: {}".format(f))
            proc_list.extend(find_processes_in_LXCat(f))

        return proc_list

    try:
        read = open(filename, 'r')
        read.close()
    except FileNotFoundError:
        print("The Xsec file {} could not be opened".format(filename))
        return [];


    with open(filename, 'r') as reader:

        for i, line in enumerate(reader):
            if "PROCESS:" in line:
                #print(line)
                proc_list.append(line.strip("PROCESS:").strip())

    # for every known excitation, also pass in the presumed existing superelastic
    sup_list = []

    for proc in proc_list:
        if "Excitation" in proc:
            reactant = reactant_from_process(proc)
            product = product_from_process(proc)

            sup_list.append("E + {} -> E + {}, Superelastic (derived)".format(product, reactant))

    proc_list.extend(sup_list)

    return  remove_duplicate_species(list(dict.fromkeys(proc_list)))




##--- trial script
if __name__ == "__main__":
    Xsec_fids = ["Cross-sections/Biagi_N2.txt", "Cross-sections/Biagi_SF6.txt"];

    print("Running as script, debug mode by reading test file: {}".format(Xsec_fids))
    print("Found the following unique species:")
    specs = find_species_in_LXCat(Xsec_fids)
    for s in specs:
        print(s)

    print("Found the following unique processes:")
    procs = find_processes_in_LXCat(Xsec_fids)
    for p in procs:
        print(p)

    print("Leaving test script")   