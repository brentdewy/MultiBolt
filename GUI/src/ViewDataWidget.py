import configparser
import const as c 

from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QPushButton, QTableWidgetItem, QWidget, QListWidget, QMainWindow, QHBoxLayout, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QMessageBox, QFileDialog, QVBoxLayout, QTableWidget, QCheckBox, QTextEdit, QTreeView, QFileSystemModel

import shutil
import os



class ViewDataWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()
        self.set_defaults()
    
    def home(self):
        self.model = QFileSystemModel(self)
        self.model.setRootPath(c.HOME)

        self.viewingTree = QTreeView(self)
        self.viewingTree.setModel(self.model)
        self.viewingTree.setRootIndex(self.model.index(c.HOME))
        self.viewingTree.setColumnHidden(1, True)
        self.viewingTree.setColumnHidden(2, True)
        self.viewingTree.setColumnHidden(3, True)

        self.txtViewer = QTextEdit(self)
        self.txtViewer.setReadOnly(True)

        #self.clearAllExport = QPushButton("Erase All Folders")

        #self.clearOneExport = QPushButton("Erase Selected Folder")
        
        self.changeExportView = QPushButton("Change View")
        
        self.currentExportView = QLineEdit(self)
        self.currentExportView.setReadOnly(True)
        
        #self.setSizes(self.WINDOW_WIDTH, self.WINDOW_HEIGHT)

        
        pass

    def connects(self):
        self.viewingTree.selectionModel().selectionChanged.connect(self.handletxtViewer)
        #self.clearAllExport.clicked.connect(self.clearAllExportFunc)
        #self.clearOneExport.clicked.connect(self.clearOneExportFunc)
        self.changeExportView.clicked.connect(self.handleChangeDir)
        self.currentExportView.textChanged.connect(lambda: self.model.setRootPath(self.currentExportView.text()))

        pass

    def layout(self):
        layout = QVBoxLayout()

        topHbox = QHBoxLayout()
        topHbox.addWidget(self.changeExportView)
        topHbox.addWidget(self.currentExportView)
        #leftvbox.addWidget(self.clearOneExport)
        #leftvbox.addWidget(self.clearAllExport)
        
        bottomHbox = QHBoxLayout()
        bottomHbox.addWidget(self.viewingTree)
        bottomHbox.addWidget(self.txtViewer)

        layout.addLayout(topHbox)
        layout.addLayout(bottomHbox)
        self.setLayout(layout)

        pass


    def handleChangeDir(self):
        filename = QFileDialog.getExistingDirectory(self, 'Open Dir', c.HOME)
        if filename == "":
            return

        self.currentExportView.setText(filename)

        self.model.setRootPath(filename)
        self.viewingTree.setModel(self.model)
        self.viewingTree.setRootIndex(self.model.index(filename))



    def handletxtViewer(self):
        self.txtViewer.clear()

        try:
            index = self.viewingTree.selectionModel().selection().indexes()[0]
        except:
            return

        path = self.model.filePath(index)

        #print(os.path.splitext(path)[1])

        if os.path.splitext(path)[1] == '.txt':
            self.txtViewer.clear()
            with open(path,'r') as fh:
                all_lines = fh.readlines()

            for line in all_lines:
                self.txtViewer.insertPlainText(line)

        return

    def set_defaults(self):

        try:

            config = configparser.ConfigParser()
            config.read(c.ini_name)

            default = config['Default']

            self.model.setRootPath(c.HOME)
            path = (c.EXPORT_PATH).replace("\\", "/")
            self.viewingTree.setRootIndex(self.model.index(path))

            self.currentExportView.setText(path)

        except:
            return
      


if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = ViewDataWidget(window)

    x.show()
 
    sys.exit(app.exec_())