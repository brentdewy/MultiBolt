A pre-compiled static library of OpenBLAS is provided for the convenience of new users.
For MultiBolt's purposes, link at compile time, alongisde SuperLU.
As is the case with all pre-compiled objects, USE AT YOUR OWN RISK.
Compiled using MSVC 2019 on a Windows 10 machine.
-Max Flynn, 01/18/2022

'libblas.a' is a pre-compiled static BLAS (not openblas) library made through the normal compilation process of SuperLU.
As is the case with all pre-compiled objects, USE AT YOUR OWN RISK.
Compiled using gcc-9 suite on an OpenSuse machine.
As it was compiled using SuperLU's build process, 'libblas.a' is subject to the same redistribution rules.
See 'lib/SuperLU' for more information. 
-Max Flynn, 03/25/2022