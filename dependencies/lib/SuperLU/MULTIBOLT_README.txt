
SuperLU is a sparse matrix solver which MultiBolt relies on to let Armadillo solve sparse matrix systems.

Armadillo strictly requires that only superlu 5.2 may be used.
https://github.com/xiaoyeli/superlu.git Check git repository, use branch for 5.2.x, which at time of writing is called 'maint'.

In SuperLU_archive, I have attached a zip-file of the exact version known to compile as of January 2022 for safe-keeping.
To build your own superlu.lib, un-zip this archive and build as if from source.

Currently, MultiBolt links with SuperLU by Armadillo expecting to be linked against a compiled static library.
Meaning, if one builds MultiBolt, expect to either build or acquire a SuperLU.lib (static library object) file, and to link with this file explicitly.

Remember that compiled libraries are not cross-platform.

In the interest of streamlining this for the user, I have pre-compiled a library on Windows 10 (x64)
See: SuperLU_build/superlu_5.2.2_win64.lib
This should be in this same directory (lib/SuperLU), alongside the appropriate SuperLU license statement.
This was compiled in-house using the BLAS libraries on hand (OpenBLAs 0.3.19). USE AT YOUR OWN RISK.

MultiBolt in no way owns or influences SuperLU; check the SuperLU dist and its license before you re-distribute SuperLU_win64.lib, or similar, in any way.

-Max Flynn, 11/2021




'libsuperlu.a' is a pre-compiled static superlu library for linux environments.
Compiled using the internal blas libraries native to SuperLU's normal build.
As is the case with all pre-compiled binaries, USE AT YOUR OWN RISK.
Compiled using gcc-9 on an OpenSuse machine.

-Max Flynn, 03/25/2022