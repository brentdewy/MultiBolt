// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

// VS project set aside for messing around with MultiBolt and CollisionLibrary as a dev

#include <iostream>
#include <multibolt>


int main() {

	std::cout << "hello world!" << std::endl;

	return 0;

}