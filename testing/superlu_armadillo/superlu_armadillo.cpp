#define ARMA_USE_SUPERLU

#define ARMA_DONT_USE_LAPACK

#include <armadillo>

int main(){
    
    using namespace arma;
    
    // example code taken from arma documentation of spsolve()
    
    sp_mat A = sprandu<sp_mat>(1000, 1000, 0.1);

    vec b(1000,    fill::randu);
    //mat B(1000, 5, fill::randu);

    vec x(1000, fill::zeros);
    //vec x = spsolve(A, b);  // solve one system
    //mat X = spsolve(A, B);  // solve several systems

    ///// bool status = spsolve(x, A, b);  // use default solver
    //if(status == false)  { cout << "no solution" << endl; }

    ///  //// //spsolve(x, A, b, "lapack" );  // use LAPACK  solver
    spsolve(x, A, b, "superlu");  // use SuperLU solver

    superlu_opts opts;

    opts.allow_ugly  = true;
    opts.equilibrate = true;

    spsolve(x, A, b, "superlu", opts);
    
    x.print();
    
    std::cout << "MultiBolt:: test of superlu + armadillo. SUCCESS." << std::endl;
    
    return 0;
}
