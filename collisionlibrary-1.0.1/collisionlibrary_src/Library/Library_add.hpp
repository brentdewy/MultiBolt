// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_LIBRARYADD
#define COLLISIONLIBRARY_LIBRARYADD
#include "collisionlibrary"


void lib::Library::add_species_from_LXCat_file(const std::string& Xsec_fid) {


	// first, identify the species that exist in the file
	auto found_pairs = lib::identify_species_in_LXCat_file(Xsec_fid);

	// and the ones that you already have on hand
	std::vector<std::string> existing_species = {};
	for (auto spec : allspecies) {
		existing_species.push_back(spec->_name);
	}


	// add new species libraries to this object from the map above (if they don't exist already).
	for (auto it = found_pairs.begin(); it != found_pairs.end(); ++it) {

		auto name = *it;
		

		// if is not already in vector
		if (std::count(existing_species.begin(), existing_species.end(), name) == 0) {

			add_blank_species(name);

		}
	}

}



void lib::Library::add_xsecs_from_LXCat_files(const std::string& Xsec_fid) {

	add_species_from_LXCat_file(Xsec_fid);

	auto xsecs = lib::make_Xsecs_from_LXCat_file(Xsec_fid);


	for (auto spec : allspecies) {
		for (auto x : xsecs) {

			if (lib::same_string(x->reactant(), spec->_name)) {

				spec->add_Xsec(x);
			}
		}
	}

	return;

}

void lib::Library::add_xsecs_from_LXCat_files(const std::vector<std::string>& Xsec_fids) {
	for (auto& str : Xsec_fids) {
		add_xsecs_from_LXCat_files(str);
	}
}


void lib::Library::add_blank_species(const std::string& name) {

	// does the species already exist?
	if (!has_species(name)) {
		allspecies.push_back(std::make_shared<lib::Species>(lib::Species(name)));
	}
	else {

		lib::normal_statement("Warning: The species with name [" + name + "] was not added because it already exists in the library.");
	}

}





void lib::Library::add_blank_species(const std::vector<std::string>& names) {

	for (auto str : names) {

		add_blank_species(str);

	}


}


void lib::Library::add_Xsec(std::shared_ptr<lib::AbstractXsec> x) {


	// do not add Xsec if it is not fully defined!
	if (x->code() == lib::CollisionCode::nocollision) {
		lib::normal_statement("The Xsec with process [" + x->process() + "] was not added to the library because it was defined as 'nocollision'.");
		return;
	}


	// assume the Xsec has a valid reactant name, add it to that species

	auto ptr  = get_species(x->reactant());
	if (ptr == nullptr) {
		add_blank_species(x->reactant());
		lib::normal_statement("To add the Xsec with process [" + x->process() + "] to the library, the species with name [" + x->reactant() + "] was also added.");

	}

	ptr = get_species(x->product());
	if (ptr == nullptr) {
		add_blank_species(x->product());
		lib::normal_statement("To add the Xsec with process [" + x->process() + "] to the library, the species with name [" + x->product() + "] was also added.");

	}
	

	ptr = get_species(x->reactant());
	ptr->add_Xsec(x);

	lib::debug_statement("Xsec with process [" + x->process() + "] added to the library.");


	return;
	
}

#endif