// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_MAKEXSECSFROMLXCAT
#define COLLISIONLIBRARY_MAKEXSECSFROMLXCAT
#include "collisionlibrary"

// Collect a big vector of Xsecs which can be found int he file
std::vector<std::shared_ptr<lib::AbstractXsec>> make_Xsecs_from_LXCat_file(const std::string& Xsec_fid) {



	std::vector<double> g_placeholder;


	std::vector<std::shared_ptr<lib::AbstractXsec>> xsecs;


	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;

	ifstream fin; // file reading stream
	stringstream ss; // message output stream
	fs::path fpath = Xsec_fid;

	// fresh-start from file beginning
	fin.open(fpath.c_str());


	if (!fin) { // if the file can't be opened
		lib::file_not_opened_err(Xsec_fid);

		return xsecs;
	}

	std::string line = ""; // placeholder, filled while reading

	std::vector<std::string> raw_header = {};

	std::string database; // may change
	std::string date_retrieved;

	// parent file is fpath

	while (getline(fin, line)) {

		raw_header = {};

		// parse date-retrieved out of a line that may show up
		if (line.find("Generated on") != std::string::npos) {
			auto vec1 = lib::split(line, ". All rights reserved.");
			auto vec2 = lib::split(vec1.front(), "Generated on ");
			date_retrieved = lib::trim_copy(vec2.back());
		}


		// parse current database indicator out of a line that may show up
		if (line.find("PERMLINK") != std::string::npos) {
			auto vec = lib::split(line, "www.lxcat.net/");
			database = lib::trim_copy(vec.back());
		}




		// todo: this is your chance to chunk-in the raw-header instead
		if (lib::is_collision_string(line)) {

			do {

				raw_header.push_back(line);

			} while (line.find("COLUMNS") == std::string::npos && getline(fin, line));

			auto i = lib::LXCatHeader(raw_header, database, date_retrieved, Xsec_fid);

			// next line should just be the dashes, an be ignored
			getline(fin, line);
			getline(fin, line);


			std::vector<double> energy, sigma;
			do {

				auto numbers = lib::split(line, "\t");

				energy.push_back(stod(numbers[0]));
				sigma.push_back(stod(numbers[1]));

			} while (getline(fin, line) && line.find("---") == std::string::npos);


			auto parsed = lib::parse_LXCat_info_from_raw_header(raw_header);

			// the name of the reactant denotes which species this will belong to
			// find the species in 'all'

			//for (auto spec : all) {
			std::string reactant = std::get<1>(parsed);
			std::string product = std::get<2>(parsed);

			std::string process = std::get<6>(parsed);

			//if (lib::same_string(name, spec->name)) { // find based on reactant name

			auto code = std::get<0>(parsed);
			double Mratio = std::get<3>(parsed);
			double eV_thresh = std::get<4>(parsed);

			double g = std::get<5>(parsed);

			

			std::shared_ptr<lib::AbstractXsec> x;


			switch (code) { // switch based on code

			case lib::CollisionCode::attachment:
				x = std::make_shared<lib::DiscreteTotalAttachment>(lib::DiscreteTotalAttachment(energy, sigma, reactant, product, process ));
				break;

			case lib::CollisionCode::elastic:
				x = std::make_shared<lib::DiscreteTotalElasticMomentumTransfer>(lib::DiscreteTotalElasticMomentumTransfer(energy, sigma, Mratio, reactant, product, process));
				break;

			case lib::CollisionCode::effective:
				x = std::make_shared<lib::DiscreteTotalEffectiveMomentumTransfer>(lib::DiscreteTotalEffectiveMomentumTransfer(energy, sigma, Mratio, reactant, product, process));
				break;

			case lib::CollisionCode::excitation: {

				auto exc = lib::DiscreteTotalExcitation(energy, sigma, eV_thresh, reactant, product, process);
				exc.info(std::make_shared<lib::LXCatHeader>(i));

				x = std::make_shared<lib::DiscreteTotalExcitation>(exc);

				
				if (lib::same_string(reactant, product)) {
					lib::normal_statement("Warning: inelastic process '" + process + "' has the same species for both its reactant and product.");
					lib::normal_statement("\tThe superelastic collsision will not be derived at-all, to avoid enexpected behavior.");
					lib::normal_statement("\tTo avoid this, force the name of the reactant and product to be different by editing the cross section file.");
				}
				else {
					lib::DerivedTotalSuperelastic sup = lib::DerivedTotalSuperelastic(std::make_shared<lib::DiscreteTotalExcitation>(exc), g);

					xsecs.push_back(std::make_shared<lib::DerivedTotalSuperelastic>(sup));
				}

				break;
			}
			case lib::CollisionCode::ionization:
				x = std::make_shared<lib::DiscreteTotalIonization>(lib::DiscreteTotalIonization(energy, sigma, eV_thresh, reactant, product, process));

				break;

			}

			if (x->code() != lib::CollisionCode::nocollision) {

				x->info(std::make_shared<lib::LXCatHeader>(i));
				xsecs.push_back(x);
			}
		}
	}


	fin.close();

	return xsecs;
}

#endif

