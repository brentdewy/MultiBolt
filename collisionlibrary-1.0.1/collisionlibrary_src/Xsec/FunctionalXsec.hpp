// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_FUNCTIONALXSEC
#define COLLISIONLIBRARY_FUNCTIONALXSEC
#include "collisionlibrary"

// For analyitcally defined Xsecs
// In this form, define a std::function foo
// Expects one double in, and one double out.
class FunctionalXsec : virtual public lib::AbstractXsec {

public:
	std::function<double(double)> foo;

	FunctionalXsec() {  };
	FunctionalXsec(std::function<double(double)>& foo, lib::CollisionCode code) {
		this->code(code);
		this->foo = foo;
	}

	void print() { return; }


	double eval_at_eV(const double eV) {
		return foo(eV);
	}

	arma::colvec eval_at_eV(const arma::colvec& eV) {
		arma::colvec temp = arma::colvec(eV.n_elem, arma::fill::zeros);

		arma::sword k = 0;
		for (double e : eV) {
			temp(k) = foo(e);
			++k;
		}
		return temp;
	}

	// a note: this can be slow compared to setting off of tabulated data
	// purely because this assumes a sole double in : double out
	void set_grid(const int gkey, const arma::colvec& eV) {

		gridded_s[gkey] = arma::colvec(eV.n_elem, arma::fill::zeros);


		gridded_s[gkey] = eval_at_eV(eV);
		gridded_s[gkey] = gridded_s[gkey] * _scale;

		lib::debug_statement("Xsec with process [" + process() + "] gridded with gkey [" + std::to_string(gkey) + "].");
	}

};

#endif

