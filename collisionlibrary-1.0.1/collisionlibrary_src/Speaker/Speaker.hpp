// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_SPEAKER
#define COLLISIONLIBRARY_SPEAKER
#include "collisionlibrary"


class Speaker {

private:
	lib::OutputOption outOp = lib::OutputOption::NORMAL;

public:
	void printmode_normal_statements() { this->outOp = lib::OutputOption::NORMAL; return; }
	void printmode_debug_statements() { this->outOp = lib::OutputOption::DEBUG; return; }
	void printmode_no_statements() { this->outOp = lib::OutputOption::SILENT; return; }

	lib::OutputOption get_outOp() { return outOp; }
};

Speaker libSpeaker = Speaker(); // Initially using debug mode. To change, call libSpeaker.printmode_normal_statements(), etc.

#endif