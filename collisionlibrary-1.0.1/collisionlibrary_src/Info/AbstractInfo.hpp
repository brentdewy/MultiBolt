// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_ABSTRACTINFO
#define COLLISIONLIBRARY_ABSTRACTINFO


#include "collisionlibrary"



// An 'info' is the object held by a Xsec which is used to print its reference.
// Possessing an 'info' is not strictly required to evaluate a cross-section.
class AbstractInfo {
public:


	virtual void print() = 0; 

	std::string _reference;

	std::string reference() { return _reference; }

};

#endif





