// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 
#pragma once
#ifndef COLLISIONLIBRARY_PARSELXCAT
#define COLLISIONLIBRARY_PARSELXCAT


#include "collisionlibrary"



// Collect all data (if applicable) that could possibly by found in the collision-centric header
// in an LXCat data file
std::tuple<lib::CollisionCode, std::string, std::string, double, double, double, std::string>
	parse_LXCat_info_from_raw_header(std::vector<std::string> raw_header) {

	/* tuple index is parsed in:
	0->code
	1->reactant
	2->product
	3->Mratio
	4->threshold
	5->g
	6->process
	*/

	std::tuple<lib::CollisionCode, std::string, std::string, double, double, double, std::string> tup;

	auto code = lib::collision_str_code_map.find(raw_header[0])->second;


	// get reactant
	std::string line = raw_header[1];
	std::string delimiter;
	if (line.find("<->") != std::string::npos) {
		delimiter = "<->";
	}
	else {
		delimiter = "->";
	}
	std::string reactant = lib::trim_copy(lib::split(raw_header[1], delimiter)[0]);

	// get product using same delimiter
	auto vec = lib::split(raw_header[1], delimiter);
	std::string product = lib::trim_copy(vec[vec.size() - 1]);


	// parsing special - Mratio, thersh, g

	std::vector<std::string> ds = lib::split(lib::trim_copy(raw_header[2]), " ");

	// remove "empty entries"
	int Nsize = ds.size();
	for (int i = 0; i < Nsize; ++i) {
		if (lib::same_string("", ds[i])) {
			ds.erase(ds.begin() + i);
			--i;
			--Nsize;
		}
	}

	double Mratio = 0;
	double g = 1.0;
	double eV_thresh = 0;

	if (code == lib::CollisionCode::elastic || code == lib::CollisionCode::effective) {
		Mratio = stod(ds[0]); // first should be ratio
	}
	else if (code == lib::CollisionCode::ionization) {
		eV_thresh = stod(ds[0]); // first should be thresh
	}
	else if (code == lib::CollisionCode::excitation) {
		eV_thresh = stod(ds[0]); // first should be thresh

		if (ds.size() > 1) {
			g = stod(ds[1]); // second should be g, if it exists
		}
		else {
			g = 1.0;
		}
	}


	// get the process string
	std::string process = lib::UNALLOCATED_STRING;
	for (auto& str : raw_header) {
		if (str.find("PROCESS") != std::string::npos) {
			std::string ds = lib::trim_copy(lib::split(lib::trim_copy(str), "PROCESS: ")[1]);
			process = ds;
			break;
		}
	}

	return std::tuple<lib::CollisionCode, std::string, std::string, double, double, double, std::string>
	{code, reactant, product, Mratio, eV_thresh, g, process};
}

#endif